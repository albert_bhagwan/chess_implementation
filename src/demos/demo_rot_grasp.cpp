#include <iostream>
#include <ros/ros.h>
#include <chesslab_setup/setobjpose.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Pose.h>

using namespace std;

// Throwaway locations (world frame) for each piece
map<int, vector<double>> piece_killed_pos = {
  {201,  {-0.13, -0.27, -0.02}},
  {202,  {-0.13, -0.33, -0.02}},
  {203,  {-0.19, -0.27, -0.02}},
  {204,  {-0.19, -0.33, -0.02}},
  {205,  {-0.19, -0.39, -0.02}},
  {206,  {-0.25, -0.27, -0.02}},
  {207,  {-0.25, -0.33, -0.02}},
  {208,  {-0.25, -0.39, -0.02}},
  {209,  {-0.31, -0.39, -0.01}}, //Rook
  {210,  {-0.37, -0.39, -0.01}}, //Rook
  {211,  {-0.31, -0.33, -0.01}}, //Knight
  {212,  {-0.37, -0.33, -0.01}}, //Knight
  {213,  {-0.31, -0.27, -0.01}}, //Bishop
  {214,  {-0.37, -0.27, -0.01}}, //Bishop
  {215,  {-0.43, -0.33,  0.00}}, //Queen
  {216,  {-0.43, -0.27,  0.00}}, //=Game Over
  {301,  { 0.13,  0.27, -0.02}},
  {302,  { 0.13,  0.33, -0.02}},
  {303,  { 0.19,  0.27, -0.02}},
  {304,  { 0.19,  0.33, -0.02}},
  {305,  { 0.19,  0.39, -0.02}},
  {306,  { 0.25,  0.27, -0.02}},
  {307,  { 0.25,  0.33, -0.02}},
  {308,  { 0.25,  0.39, -0.02}},
  {309,  { 0.31,  0.39, -0.01}}, //Rook
  {310,  { 0.37,  0.39, -0.01}}, //Rook
  {311,  { 0.31,  0.33, -0.01}}, //Knight
  {312,  { 0.37,  0.33, -0.01}}, //Knight
  {313,  { 0.31,  0.27, -0.01}}, //Bishop
  {314,  { 0.37,  0.27, -0.01}}, //Bishop
  {315,  { 0.43,  0.33,  0.00}}, //Queen
  {316,  { 0.43,  0.27,  0.00}} //=Game Over
};

geometry_msgs::Pose getCoordinates(int pieceID, string objCell) {

  geometry_msgs::Pose coords;


  if (objCell == "DEAD") {
    vector<double> drop_pos = piece_killed_pos[pieceID];
    coords.position.x = drop_pos[0];
    coords.position.y = drop_pos[1];
    coords.position.z = drop_pos[2];
  }
  else { // Goal is a board cell
    vector<string> board_letters = {"A", "B", "C", "D", "E", "F", "G", "H"};

    int cellRow = stoi(objCell.substr(1));
    string cellColStr = objCell.substr(0,1);
    vector<string>::iterator it = find(board_letters.begin(), board_letters.end(), cellColStr);
    int cellCol = distance(board_letters.begin(), it) + 1;

    // Convert Rows and Cols (1-8) to x,y
    coords.position.x = 0.05*(cellRow-4) - 0.025;
    coords.position.y = -0.05*(cellCol-4) + 0.025;

    // For z, we need half piece height
    int pieceType = pieceID%100;
    if (pieceType < 9) {
      // PAWNS
      coords.position.z = 0.04/2;
    }
    else if (pieceType < 15) {
      // ROOKS, KNIGHTS & BISHOPS
      coords.position.z = 0.06/2;
    }
    else {
      // QUEENS & KINGS
      coords.position.z = 0.08/2;
    }
  }

  // Fixed orientation
  coords.orientation.x = 0;
  coords.orientation.y = 0;
  coords.orientation.z = 0;
  coords.orientation.w = 1;

  return coords;
}

void callSetObjPose(int pieceID, string goalPos,
                    ros::ServiceClient setPoseClient) {

  chesslab_setup::setobjpose setobjpose_srv;

  setobjpose_srv.request.objid = pieceID;
  setobjpose_srv.request.p = getCoordinates(pieceID, goalPos);
  setPoseClient.call(setobjpose_srv);

}

int main( int argc, char** argv ) {
    ros::init(argc, argv, "demo_kill");
    ros::NodeHandle node;

    //Move piece
    ros::service::waitForService("/chesslab_setup/setobjpose");
    ros::ServiceClient setobjpose_client = node.serviceClient<chesslab_setup::setobjpose>("/chesslab_setup/setobjpose");

    // White Pawn 6 dead
    callSetObjPose(306, "DEAD", setobjpose_client);

    // Black Pawn 5 dead
    callSetObjPose(205, "DEAD", setobjpose_client);

    // Black Queen to D4;
    callSetObjPose(215, "D4", setobjpose_client);

}
