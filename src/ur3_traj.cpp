#include <ros/ros.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <chess_implementation/RobotConfig.h>
#include <chess_implementation/SetTrajectory.h>
#include <chess_implementation/SetTolerance.h>
#include <chess_implementation/CheckMovAction.h>
#include <chess_implementation/SetRobot.h>
#include <chesslab_setup/attachobs2robot.h>
#include <chesslab_setup/dettachobs.h>
#include <gazebo_ros_link_attacher/Attach.h>
#include <chesslab_setup/setrobconf.h>
#include <std_srvs/Empty.h>
#include <tf2_ros/transform_listener.h>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/JointState.h>
#include <tf/LinearMath/Matrix3x3.h>
#include "geometry_msgs/Quaternion.h"
#include "tf/transform_datatypes.h"
#include <geometry_msgs/Pose.h>
#include <ur3ik/UR3IK.h>
#include <sstream>

// Global variables
control_msgs::FollowJointTrajectoryGoal goalTraj;
ros::Duration traj_duration;
bool start;
tf2_ros::Buffer tfBuffer;
geometry_msgs::TransformStamped base_tool_tfStamped;
typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> Client;
Client *robotClientA;
Client *robotClientB;
ros::ServiceClient ur3ik_client;
ros::ServiceClient setrobconf_client;
sensor_msgs::JointState current_state_A;
sensor_msgs::JointState current_state_B;
double joint_increment = 0.5;
bool finished;
int error_code;
bool attach_required;
bool dettach_required;
int arucoID;
std::string chess_piece;
std::string robot_name = "team_B";

std::vector<double> home_pos = {-1.82, 0.5, -0.78, -1.57, -2.1, 1.57, 0.0};

static bool abs_compare(double a, double b)
{
    return (std::fabs(a) < std::fabs(b));
}

std::vector<double> sum_increment(std::vector<double> current, std::vector<double> delta, double it){

  std::vector<double> result(7);

  result[0] = current[0] + delta[0]*it;
  result[1] = current[1] + delta[1]*it;
  result[2] = current[2] + delta[2]*it;
  result[3] = current[3] + delta[3]*it;
  result[4] = current[4] + delta[4]*it;
  result[5] = current[5] + delta[5]*it;
  result[6] = current[6] + delta[6]*it;

  return result;
}

// A callback function when a new joint state is received for robot A
void jointStateReceivedTeamA(const sensor_msgs::JointState& msg) {

  // Some messages arrive from chesslab_setup_node when detecting collisions
  // We want to filter them so we check if the joint name format is the same as expected
  if (msg.name[0] == "team_A_elbow_joint") {
    current_state_A = msg;
  }
}

// A callback function when a new joint state is received for robot B
void jointStateReceivedTeamB(const sensor_msgs::JointState& msg) {

  // Some messages arrive from chesslab_setup_node when detecting collisions
  // We want to filter them so we check if the joint name format is the same as expected
  if (msg.name[0] == "team_B_elbow_joint") {
    current_state_B = msg;
  }

}

// DEFINED SERVICES //
bool setRobot(
  chess_implementation::SetRobot::Request &req,
  chess_implementation::SetRobot::Response &resp){

  robot_name = req.robot_name;

  return true;
}

bool jointLinTraj(
  chess_implementation::SetTrajectory::Request &req,
  chess_implementation::SetTrajectory::Response &resp){

  ROS_INFO("Setting joint-space linear trajectory");

  goalTraj.trajectory.joint_names.clear();
  goalTraj.trajectory.points.clear();
  goalTraj.path_tolerance.clear();
  goalTraj.goal_tolerance.clear();
  finished = false;

  sensor_msgs::JointState current_state;

  if (robot_name == "team_A") {
    current_state = current_state_A;
  }
  else if (robot_name == "team_B") {
    current_state = current_state_B;
  }
  else{
    ROS_ERROR("Incorrect robot name...");
    return false;
  }

  std::vector<double> delta_joints;
  std::vector<double> final_pos;
  std::vector<double>::iterator max_idx;

  if (!req.return_home) {
    arucoID = req.arucoID;
    chess_piece = req.chess_piece;

    geometry_msgs::PoseStamped tcp_world;
    tcp_world.pose = req.final_config.tcp;
    tcp_world.header.frame_id = "world";
    tcp_world.header.stamp = ros::Time::now();

    geometry_msgs::PoseStamped tcp_base_link;

    try{
      tfBuffer.transform(tcp_world, tcp_base_link, robot_name + "_base_link");
    }
    catch (tf2::TransformException &ex) {
      ROS_WARN("%s",ex.what());
      ros::Duration(1.0).sleep();
      return false;
    }

    // Compute joint coordinates for goal position
    ur3ik::UR3IK ur3ik_srv;

    ur3ik_srv.request.theta_ref.resize(6);
    ur3ik_srv.request.theta_min.resize(6);
    ur3ik_srv.request.theta_max.resize(6);
    for(int j=0; j<6; j++) {
        ur3ik_srv.request.theta_ref[j] = 0.0;
        ur3ik_srv.request.theta_min[j] =-M_PI;
        ur3ik_srv.request.theta_max[j] = M_PI;
    }

    ur3ik_srv.request.pose = tcp_base_link.pose;

    ur3ik_client.call(ur3ik_srv);

    if(ur3ik_srv.response.status) {

      double min_diff = 1e6;
      int min_idx = 0;
      for (int k = 0; k < ur3ik_srv.response.ik_solution.size(); k++) {

        double diff =
         std::fabs(current_state.position[0] - ur3ik_srv.response.ik_solution[k].ik[2]) +
         std::fabs(current_state.position[2] - ur3ik_srv.response.ik_solution[k].ik[1]) +
         std::fabs(current_state.position[3] - ur3ik_srv.response.ik_solution[k].ik[0]) +
         std::fabs(current_state.position[4] - ur3ik_srv.response.ik_solution[k].ik[3]) +
         std::fabs(current_state.position[5] - ur3ik_srv.response.ik_solution[k].ik[4]) +
         std::fabs(current_state.position[6] - ur3ik_srv.response.ik_solution[k].ik[5]);

        if (diff < min_diff){
          min_diff = diff;
          min_idx = k;
        }
      }

      final_pos.resize(7);
      final_pos[0] = ur3ik_srv.response.ik_solution[min_idx].ik[2];
      final_pos[1] = req.final_config.gripper;
      final_pos[2] = ur3ik_srv.response.ik_solution[min_idx].ik[1];
      final_pos[3] = ur3ik_srv.response.ik_solution[min_idx].ik[0];
      final_pos[4] = ur3ik_srv.response.ik_solution[min_idx].ik[3];
      final_pos[5] = ur3ik_srv.response.ik_solution[min_idx].ik[4];
      final_pos[6] = ur3ik_srv.response.ik_solution[min_idx].ik[5];
    }
    else{
        ROS_ERROR("Unable to compute the IK of the goal");
        return false;
    }
  }
  else{
    final_pos = home_pos;
  }
  delta_joints.resize(7);
  delta_joints[0] = final_pos[0] - current_state.position[0];
  delta_joints[1] = final_pos[1] - current_state.position[1];
  delta_joints[2] = final_pos[2] - current_state.position[2];
  delta_joints[3] = final_pos[3] - current_state.position[3];
  delta_joints[4] = final_pos[4] - current_state.position[4];
  delta_joints[5] = final_pos[5] - current_state.position[5];
  delta_joints[6] = final_pos[6] - current_state.position[6];

  max_idx = std::max_element(delta_joints.begin(), delta_joints.end(), abs_compare);

  double cycletime = std::ceil(std::fabs(delta_joints[std::distance(delta_joints.begin(), max_idx)])/0.75);
  double moveduration = 0.2;
  int trajpoints = cycletime/moveduration;

  ROS_INFO_STREAM("Total cycle time: "  << cycletime);
  ROS_INFO_STREAM("Total trajectory points: "  << trajpoints);

  //Set goal trajectory
  goalTraj.trajectory.joint_names.resize(7);
  goalTraj.trajectory.joint_names[0] = robot_name + "_elbow_joint";
  goalTraj.trajectory.joint_names[1] = robot_name + "_gripper_right_driver_joint";
  goalTraj.trajectory.joint_names[2] = robot_name + "_shoulder_lift_joint";
  goalTraj.trajectory.joint_names[3] = robot_name + "_shoulder_pan_joint";
  goalTraj.trajectory.joint_names[4] = robot_name + "_wrist_1_joint";
  goalTraj.trajectory.joint_names[5] = robot_name + "_wrist_2_joint";
  goalTraj.trajectory.joint_names[6] = robot_name + "_wrist_3_joint";

  goalTraj.trajectory.points.resize(trajpoints);
  goalTraj.trajectory.points[0].positions.resize(7);
  goalTraj.trajectory.points[0].positions =
          sum_increment(current_state.position, delta_joints, 1.0/trajpoints);
  //ROS_INFO_STREAM("Desired path is " << goalTraj.trajectory.points[0].positions[2]);

  goalTraj.trajectory.points[0].time_from_start = ros::Duration(moveduration);

  for(int i=1; i<trajpoints;i++) {
    goalTraj.trajectory.points[i].positions.resize(7);
    goalTraj.trajectory.points[i].positions =
            sum_increment(current_state.position, delta_joints, (i+1.0)/trajpoints);
    //ROS_INFO_STREAM("Desired path is " << goalTraj.trajectory.points[i].positions[2]);

    goalTraj.trajectory.points[i].time_from_start = goalTraj.trajectory.points[i-1].time_from_start + ros::Duration(moveduration);
  }

  //set last point velocity and acceleration to zero
  goalTraj.trajectory.points[trajpoints-1].velocities.resize(7);
  goalTraj.trajectory.points[trajpoints-1].velocities[0] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[1] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[2] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[3] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[4] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[5] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[6] = 0.0;

  goalTraj.trajectory.points[trajpoints-1].accelerations.resize(7);
  goalTraj.trajectory.points[trajpoints-1].accelerations[0] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[1] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[2] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[3] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[4] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[5] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[6] = 0.0;

  return true;
}

bool carteLinTraj(
  chess_implementation::SetTrajectory::Request &req,
  chess_implementation::SetTrajectory::Response &resp){

  ROS_INFO("Setting operational space linear trajectory");

  goalTraj.trajectory.joint_names.clear();
  goalTraj.trajectory.points.clear();
  goalTraj.path_tolerance.clear();
  goalTraj.goal_tolerance.clear();
  finished = false;

  sensor_msgs::JointState current_state;

  if (robot_name == "team_A") {
    current_state = current_state_A;
  }
  else if (robot_name == "team_B") {
    current_state = current_state_B;
  }
  else{
    ROS_ERROR("Incorrect robot name...");
    return false;
  }

  // Id of piece being moved
  arucoID = req.arucoID;
  chess_piece = req.chess_piece;

  geometry_msgs::PoseStamped tcp_world;
  tcp_world.pose = req.final_config.tcp;
  tcp_world.header.frame_id = "world";
  tcp_world.header.stamp = ros::Time::now();

  geometry_msgs::PoseStamped tcp_base_link;

  try{
    tfBuffer.transform(tcp_world, tcp_base_link, robot_name + "_base_link");
  }
  catch (tf2::TransformException &ex) {
    ROS_WARN("%s", ex.what());
    ros::Duration(1.0).sleep();
    return false;
  }

  // Pose is X Y Z R P Y Gripper

  // Express goal pose as XYZRPYG
  std::vector<double> goal_pose;
  tf::Quaternion quat;

  goal_pose.resize(7);
  goal_pose[0] = tcp_base_link.pose.position.x;
  goal_pose[1] = tcp_base_link.pose.position.y;
  goal_pose[2] = tcp_base_link.pose.position.z;
  tf::quaternionMsgToTF(tcp_base_link.pose.orientation, quat);
  tf::Matrix3x3(quat).getRPY(goal_pose[3], goal_pose[4], goal_pose[5]);
  ROS_INFO_STREAM("Goal [base_link frame] -> Roll: " << goal_pose[3] << "; Pitch: " << goal_pose[4] << "; Yaw: "  << goal_pose[5]);
  goal_pose[6] = req.final_config.gripper;


  // Get current pose (XYZRPYG) from joints
  std::vector<double> current_pose(7);
  try{
    base_tool_tfStamped = tfBuffer.lookupTransform(robot_name + "_base_link", robot_name + "_tool0",
                              ros::Time(0));

    current_pose[0] = base_tool_tfStamped.transform.translation.x;
    current_pose[1] = base_tool_tfStamped.transform.translation.y;
    current_pose[2] = base_tool_tfStamped.transform.translation.z;

    tf::quaternionMsgToTF(base_tool_tfStamped.transform.rotation, quat);
    tf::Matrix3x3(quat).getRPY(current_pose[3], current_pose[4], current_pose[5]);

    current_pose[6] = current_state.position[1];

    // Debug info:
    ROS_INFO_STREAM("Current [base_link frame] -> X: " << current_pose[0] << "; Y: "
                            << current_pose[1] << "; Z: " << current_pose[2]);
    ROS_INFO_STREAM("Current [base_link frame] -> Roll: " << current_pose[3] << "; Pitch: "
                            << current_pose[4] << "; Yaw: " << current_pose[5]);
    ROS_INFO_STREAM("Current gripper state: " << current_pose[6]);
  }
  catch (tf2::TransformException &ex) {
    ROS_WARN("%s",ex.what());
    ros::Duration(1.0).sleep();
    return false;
  }

  std::vector<double> delta_pose;
  std::vector<double>::iterator max_rot_idx;
  double op_space_distance;

  // Displacement in operational space (XYZRPYG)
  delta_pose.resize(7);
  delta_pose[0] = goal_pose[0] - current_pose[0];
  delta_pose[1] = goal_pose[1] - current_pose[1];
  delta_pose[2] = goal_pose[2] - current_pose[2];
  delta_pose[3] = goal_pose[3] - current_pose[3];
  delta_pose[4] = goal_pose[4] - current_pose[4];
  delta_pose[5] = goal_pose[5] - current_pose[5];
  delta_pose[6] = goal_pose[6] - current_pose[6];

  // Impose null variation if roll is pi or -pi, as it may lead to unnecessary turns
  if (std::fabs(goal_pose[3]) - std::fabs(current_pose[3]) <= 0.01) {
    delta_pose[3] = 0.0;
  }

  op_space_distance = sqrt(delta_pose[0]*delta_pose[0] +
                           delta_pose[1]*delta_pose[1] +
                           delta_pose[2]*delta_pose[2]);

  ROS_INFO_STREAM("Origin-goal distance: " << op_space_distance);

  double movevelocity;
  double cycletime;
  double moveduration;
  bool gripperAction = false;
  bool collision_required = false;

  // Three cases: displacement, gripper action, or rotation
  if (op_space_distance >= 0.005) {
    ROS_INFO("Displacement movement");
    movevelocity = 0.15;
    cycletime = ceil(op_space_distance/movevelocity);
    moveduration = 0.2;
    if (delta_pose[2] < -0.015) {
      collision_required = true;
    }

    if (delta_pose[6] > 0.02) {
      attach_required = true;
    }
    else if (delta_pose[6] < -0.02) {
      dettach_required = true;
    }

  }
  else if (fabs(delta_pose[6]) >= 0.02) {
    // Gripper action in 1s (not set by velocity)
    ROS_INFO("Gripper-only movement");
    cycletime = 1;
    moveduration = 0.2;
    gripperAction = true;
    if (delta_pose[6] > 0.0) {
      attach_required = true;
    }
    else{
      dettach_required = true;
    }
  }
  else {
    ROS_INFO("Rotation movement");
    movevelocity = 0.75;
    max_rot_idx = std::max_element(delta_pose.begin()+3, delta_pose.end()-1, abs_compare);
    cycletime = ceil(std::fabs(delta_pose[std::distance(delta_pose.begin() + 3, max_rot_idx)])/movevelocity);
    moveduration = 0.2;
  }
  int trajpoints = cycletime/moveduration;

  ROS_INFO_STREAM("Total cycle time: "  << cycletime);
  ROS_INFO_STREAM("Total trajectory points: "  << trajpoints);

  // Set goal trajectory
  // First, name all joints
  goalTraj.trajectory.joint_names.resize(7);
  goalTraj.trajectory.joint_names[0] = robot_name + "_elbow_joint";
  goalTraj.trajectory.joint_names[1] = robot_name + "_gripper_right_driver_joint";
  goalTraj.trajectory.joint_names[2] = robot_name + "_shoulder_lift_joint";
  goalTraj.trajectory.joint_names[3] = robot_name + "_shoulder_pan_joint";
  goalTraj.trajectory.joint_names[4] = robot_name + "_wrist_1_joint";
  goalTraj.trajectory.joint_names[5] = robot_name + "_wrist_2_joint";
  goalTraj.trajectory.joint_names[6] = robot_name + "_wrist_3_joint";

  // Resize trajectory vector to computed length
  goalTraj.trajectory.points.resize(trajpoints);

  // If not a gripper action, IK is needed (whole process)
  if (!gripperAction) {

    // Compute for the first trajectory point separately
    goalTraj.trajectory.points[0].positions.resize(7);

    // Linear increments in the operational space
    std::vector<double> pose(7);
    pose = sum_increment(current_pose, delta_pose, 1.0/trajpoints);

    // Convert first pose to joint values
    ur3ik::UR3IK ur3ik_srv;

    ur3ik_srv.request.theta_ref.resize(6);
    ur3ik_srv.request.theta_min.resize(6);
    ur3ik_srv.request.theta_max.resize(6);
    for(int j=0; j<6; j++) {
        ur3ik_srv.request.theta_ref[j] = 0.0;
        ur3ik_srv.request.theta_min[j] = -M_PI;
        ur3ik_srv.request.theta_max[j] = M_PI;
    }

    ur3ik_srv.request.pose.position.x = pose[0];
    ur3ik_srv.request.pose.position.y = pose[1];
    ur3ik_srv.request.pose.position.z = pose[2];

    quat.setRPY(pose[3], pose[4], pose[5]);

    ur3ik_srv.request.pose.orientation.x = quat.x();
    ur3ik_srv.request.pose.orientation.y = quat.y();
    ur3ik_srv.request.pose.orientation.z = quat.z();
    ur3ik_srv.request.pose.orientation.w = quat.w();

    ur3ik_client.call(ur3ik_srv);

    if(ur3ik_srv.response.status){
        double min_diff = 1e6;
        int min_idx = 0;
        for (int k = 0; k < ur3ik_srv.response.ik_solution.size(); k++) {

          // Compute diff (terms disordered). current_state[1] is the gripper!
          double diff =
           std::fabs(current_state.position[0] - ur3ik_srv.response.ik_solution[k].ik[2]) +
           std::fabs(current_state.position[2] - ur3ik_srv.response.ik_solution[k].ik[1]) +
           std::fabs(current_state.position[3] - ur3ik_srv.response.ik_solution[k].ik[0]) +
           std::fabs(current_state.position[4] - ur3ik_srv.response.ik_solution[k].ik[3]) +
           std::fabs(current_state.position[5] - ur3ik_srv.response.ik_solution[k].ik[4]) +
           std::fabs(current_state.position[6] - ur3ik_srv.response.ik_solution[k].ik[5]);

          if (diff < min_diff){
            min_diff = diff;
            min_idx = k;
          }
        }

        //ROS_WARN_STREAM("Minimum difference found is: " << min_diff);

        goalTraj.trajectory.points[0].positions[0] = ur3ik_srv.response.ik_solution[min_idx].ik[2];
        goalTraj.trajectory.points[0].positions[1] = pose[6];
        goalTraj.trajectory.points[0].positions[2] = ur3ik_srv.response.ik_solution[min_idx].ik[1];
        goalTraj.trajectory.points[0].positions[3] = ur3ik_srv.response.ik_solution[min_idx].ik[0];
        goalTraj.trajectory.points[0].positions[4] = ur3ik_srv.response.ik_solution[min_idx].ik[3];
        goalTraj.trajectory.points[0].positions[5] = ur3ik_srv.response.ik_solution[min_idx].ik[4];
        goalTraj.trajectory.points[0].positions[6] = ur3ik_srv.response.ik_solution[min_idx].ik[5];

        if (min_diff > 2.0) {
          goalTraj.trajectory.points[0].positions = current_state.position;
          ROS_WARN_STREAM("Anomaly detected, imposing current state as initial goal");
          ROS_WARN_STREAM(min_diff);
          ROS_INFO_STREAM(goalTraj);
          ROS_INFO_STREAM("Current state is: " << current_state);
        }

    }
    else{
        ROS_ERROR("Unable to compute the IK");
        goalTraj.trajectory.points[0].positions = current_state.position;
    }

    goalTraj.trajectory.points[0].time_from_start = ros::Duration(moveduration);

    for (int i = 1; i < trajpoints; i++) {
      goalTraj.trajectory.points[i].positions.resize(7);

      std::vector<double> pose(7);
      pose = sum_increment(current_pose, delta_pose, (i+1.0)/trajpoints);

      ur3ik::UR3IK ur3ik_srv;

      ur3ik_srv.request.theta_ref.resize(6);
      ur3ik_srv.request.theta_min.resize(6);
      ur3ik_srv.request.theta_max.resize(6);
      for(int j=0; j<6; j++) {
          ur3ik_srv.request.theta_ref[j] = 0.0;
          ur3ik_srv.request.theta_min[j] =-M_PI;
          ur3ik_srv.request.theta_max[j] = M_PI;
      }

      ur3ik_srv.request.pose.position.x = pose[0];
      ur3ik_srv.request.pose.position.y = pose[1];
      ur3ik_srv.request.pose.position.z = pose[2];

      quat.setRPY(pose[3], pose[4], pose[5]);

      ur3ik_srv.request.pose.orientation.x = quat.x();
      ur3ik_srv.request.pose.orientation.y = quat.y();
      ur3ik_srv.request.pose.orientation.z = quat.z();
      ur3ik_srv.request.pose.orientation.w = quat.w();

      ur3ik_client.call(ur3ik_srv);

      if(ur3ik_srv.response.status){

        double min_diff = 1e6;
        int min_idx = 0;
        for (int k = 0; k < ur3ik_srv.response.ik_solution.size(); k++) {

          double diff =
           std::fabs(goalTraj.trajectory.points[i-1].positions[0] - ur3ik_srv.response.ik_solution[k].ik[2]) +
           std::fabs(goalTraj.trajectory.points[i-1].positions[2] - ur3ik_srv.response.ik_solution[k].ik[1]) +
           std::fabs(goalTraj.trajectory.points[i-1].positions[3] - ur3ik_srv.response.ik_solution[k].ik[0]) +
           std::fabs(goalTraj.trajectory.points[i-1].positions[4] - ur3ik_srv.response.ik_solution[k].ik[3]) +
           std::fabs(goalTraj.trajectory.points[i-1].positions[5] - ur3ik_srv.response.ik_solution[k].ik[4]) +
           std::fabs(goalTraj.trajectory.points[i-1].positions[6] - ur3ik_srv.response.ik_solution[k].ik[5]);

          if (diff < min_diff){
            min_diff = diff;
            min_idx = k;
          }
        }

        //ROS_WARN_STREAM("Minimum difference found is: " << min_diff);

        goalTraj.trajectory.points[i].positions[0] = ur3ik_srv.response.ik_solution[min_idx].ik[2];
        goalTraj.trajectory.points[i].positions[1] = pose[6];
        goalTraj.trajectory.points[i].positions[2] = ur3ik_srv.response.ik_solution[min_idx].ik[1];
        goalTraj.trajectory.points[i].positions[3] = ur3ik_srv.response.ik_solution[min_idx].ik[0];
        goalTraj.trajectory.points[i].positions[4] = ur3ik_srv.response.ik_solution[min_idx].ik[3];
        goalTraj.trajectory.points[i].positions[5] = ur3ik_srv.response.ik_solution[min_idx].ik[4];
        goalTraj.trajectory.points[i].positions[6] = ur3ik_srv.response.ik_solution[min_idx].ik[5];

        if (collision_required && (i == trajpoints - 1)) {

          chesslab_setup::setrobconf setrobconf_srv;

          setrobconf_srv.request.conf.resize(14);
          if (robot_name == "team_A") {
            setrobconf_srv.request.conf[0]  =  goalTraj.trajectory.points[i].positions[3];
            setrobconf_srv.request.conf[1]  =  goalTraj.trajectory.points[i].positions[2];
            setrobconf_srv.request.conf[2]  =  goalTraj.trajectory.points[i].positions[0];
            setrobconf_srv.request.conf[3] =  goalTraj.trajectory.points[i].positions[4];
            setrobconf_srv.request.conf[4] =  goalTraj.trajectory.points[i].positions[5];
            setrobconf_srv.request.conf[5] =  goalTraj.trajectory.points[i].positions[6];
            setrobconf_srv.request.conf[6] =  goalTraj.trajectory.points[i].positions[1];
            setrobconf_srv.request.conf[7]  =  current_state_B.position[3];
            setrobconf_srv.request.conf[8]  =  current_state_B.position[2];
            setrobconf_srv.request.conf[9]  =  current_state_B.position[0];
            setrobconf_srv.request.conf[10]  =  current_state_B.position[4];
            setrobconf_srv.request.conf[11]  =  current_state_B.position[5];
            setrobconf_srv.request.conf[12]  =  current_state_B.position[6];
            setrobconf_srv.request.conf[13]  =  current_state_B.position[1];
          }
          else if (robot_name == "team_B") {
            setrobconf_srv.request.conf[0]  =  current_state_A.position[3];
            setrobconf_srv.request.conf[1]  =  current_state_A.position[2];
            setrobconf_srv.request.conf[2]  =  current_state_A.position[0];
            setrobconf_srv.request.conf[3]  =  current_state_A.position[4];
            setrobconf_srv.request.conf[4]  =  current_state_A.position[5];
            setrobconf_srv.request.conf[5]  =  current_state_A.position[6];
            setrobconf_srv.request.conf[6]  =  current_state_A.position[1];
            setrobconf_srv.request.conf[7]  =  goalTraj.trajectory.points[i].positions[3];
            setrobconf_srv.request.conf[8]  =  goalTraj.trajectory.points[i].positions[2];
            setrobconf_srv.request.conf[9]  =  goalTraj.trajectory.points[i].positions[0];
            setrobconf_srv.request.conf[10] =  goalTraj.trajectory.points[i].positions[4];
            setrobconf_srv.request.conf[11] =  goalTraj.trajectory.points[i].positions[5];
            setrobconf_srv.request.conf[12] =  goalTraj.trajectory.points[i].positions[6];
            setrobconf_srv.request.conf[13] =  goalTraj.trajectory.points[i].positions[1];
          }

          setrobconf_client.call(setrobconf_srv);

          if(setrobconf_srv.response.incollision == true) {
              ROS_INFO_STREAM( "The configuration is not collision-free" << std::endl <<
              setrobconf_srv.response.msg <<
              "The collided obstacle aruco ID is " << setrobconf_srv.response.obj);

              resp.collision_detected = true;

              goalTraj.trajectory.points.resize(i);
              trajpoints = i;
              break;
          }
          else{
              ROS_INFO_STREAM(setrobconf_srv.response.msg);
          }
        }

        if (min_diff > 2.0) {
          goalTraj.trajectory.points[i].positions = goalTraj.trajectory.points[i-1].positions;
          ROS_WARN_STREAM("Anomaly detected, imposing previous goal as new goal");
          ROS_WARN_STREAM("Diff: " << min_diff << ", i=" << i);
          ROS_INFO_STREAM(goalTraj);
        }
      }
      else{
          ROS_ERROR("Unable to compute the IK");
          goalTraj.trajectory.points[i].positions = goalTraj.trajectory.points[i-1].positions;
      }

      goalTraj.trajectory.points[i].time_from_start = goalTraj.trajectory.points[i-1].time_from_start + ros::Duration(moveduration);
    }

  }
  // If only gripper action, no IK required (no other movement)
  else{

    for (int i = 0; i < trajpoints; i++) {
      goalTraj.trajectory.points[i].positions.resize(7);

      std::vector<double> pose(7);
      pose = sum_increment(current_pose, delta_pose, (i+1.0)/trajpoints);

      goalTraj.trajectory.points[i].positions = current_state.position;
      goalTraj.trajectory.points[i].positions[1] = pose[6];

      if (i==0) {
        goalTraj.trajectory.points[i].time_from_start = ros::Duration(moveduration);
      }
      else{
        goalTraj.trajectory.points[i].time_from_start = goalTraj.trajectory.points[i-1].time_from_start + ros::Duration(moveduration);
      }
    }
  }

  //set last point velocity and acceleration to zero
  goalTraj.trajectory.points[trajpoints-1].velocities.resize(7);
  goalTraj.trajectory.points[trajpoints-1].velocities[0] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[1] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[2] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[3] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[4] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[5] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].velocities[6] = 0.0;


  goalTraj.trajectory.points[trajpoints-1].accelerations.resize(7);
  goalTraj.trajectory.points[trajpoints-1].accelerations[0] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[1] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[2] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[3] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[4] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[5] = 0.0;
  goalTraj.trajectory.points[trajpoints-1].accelerations[6] = 0.0;

  //ROS_INFO_STREAM(goalTraj);

  return true;
}

bool setTolerance(
  chess_implementation::SetTolerance::Request &req,
  chess_implementation::SetTolerance::Response &resp){

  ROS_INFO_STREAM("Setting tolerance");

  goalTraj.path_tolerance = req.path_tol;
  goalTraj.goal_tolerance = req.goal_tol;
  goalTraj.goal_time_tolerance = req.goal_time_tol;

  return true;
}

bool callAction(
  std_srvs::Empty::Request &req,
  std_srvs::Empty::Response &resp){

  start = true;

	ROS_INFO("Calling action");

  return true;
}

bool checkMovAction(
  chess_implementation::CheckMovAction::Request &req,
  chess_implementation::CheckMovAction::Response &resp){

  //ROS_INFO_STREAM("Checking if follow joint action has finished");

  resp.finished = finished;
  resp.error_code = error_code;

  return true;
}


//Callback function: Called once when the goal completes
void doneCb(const actionlib::SimpleClientGoalState& state,
            const control_msgs::FollowJointTrajectoryResultConstPtr& result){
  // ROS_INFO("Finished in state [%s]", state.toString().c_str());
  // ROS_INFO("Answer: error_code is %d", result->error_code);

  finished = true;
  error_code = result->error_code;

  if (error_code!=0) {
    ROS_ERROR("Cancelling goal because aborted...");
    if (robot_name == "team_A") {
      robotClientA->cancelGoal();
    }
    else if (robot_name == "team_B") {
      robotClientB->cancelGoal();
    }
  }
}

//Callback function: Called once when the goal becomes active
void activeCb(){
  ROS_INFO("Goal just went active");
}

//Callback function: Called every time feedback is received for the goal
void feedbackCb(const control_msgs::FollowJointTrajectoryFeedbackConstPtr& feedback){

}


//Sends the goal to the FollowJointTrajectory action server and waits for the result for traj_duration seconds
//If not able to reach the goal within timeout, it is cancelled
bool moveRobotTrajectory(){
  //Set timestamp and send goal
  goalTraj.trajectory.header.stamp = ros::Time::now();

  if (robot_name == "team_A") {
    robotClientA->sendGoal(goalTraj, &doneCb, &activeCb, &feedbackCb);
  }
  else if (robot_name == "team_B") {
    robotClientB->sendGoal(goalTraj, &doneCb, &activeCb, &feedbackCb);
  }
}

bool serviceAttachDettach(ros::ServiceClient kautham_client,
                          ros::ServiceClient gazebo_client,
                          std::string type){

  // If attach (default)
  chesslab_setup::attachobs2robot kautham_srv;
  if (type == "dettach") {
    chesslab_setup::dettachobs kautham_srv;
  }

  kautham_srv.request.robotName =  robot_name;
  kautham_srv.request.objarucoid =  arucoID;

  if (!kautham_client.call(kautham_srv)) {
    return false;
  }
  gazebo_ros_link_attacher::Attach gazebo_srv;

  gazebo_srv.request.model_name_1 = chess_piece;
  gazebo_srv.request.link_name_1 = "link";
  gazebo_srv.request.model_name_2 = robot_name + "_arm";
  gazebo_srv.request.link_name_2 = robot_name + "_gripper_right_follower";

  if (!gazebo_client.call(gazebo_srv)) {
    return false;
  }

  return true;
}

int main(int argc, char **argv){
  // Initialize the ROS system and become a node.
  ros::init(argc, argv, "ur3_traj");
  ros::NodeHandle n;

  //Define simple action client and wait for server
  robotClientA = new Client("team_A_arm/joint_trajectory_controller_with_gripper/follow_joint_trajectory");
  robotClientB = new Client("team_B_arm/joint_trajectory_controller_with_gripper/follow_joint_trajectory");

  if(!robotClientA->waitForServer(ros::Duration(5.0))){
      ROS_ERROR(" *** action server not available *** ");
  };
  if(!robotClientB->waitForServer(ros::Duration(5.0))){
      ROS_ERROR(" *** action server not available *** ");
  };

  // Subscribe to tfs
  tf2_ros::TransformListener tfListener(tfBuffer);

  // Client of UR3IK
  ros::service::waitForService("/UR3IK");
  ur3ik_client = n.serviceClient<ur3ik::UR3IK>("/UR3IK");
  ros::service::waitForService("/chesslab_setup/attachobs2robot");
  ros::ServiceClient attach_kautham_client = n.serviceClient<chesslab_setup::attachobs2robot>("/chesslab_setup/attachobs2robot");
  ros::service::waitForService("/link_attacher_node/attach");
  ros::ServiceClient attach_gazebo_client = n.serviceClient<gazebo_ros_link_attacher::Attach>("/link_attacher_node/attach");
  ros::service::waitForService("/chesslab_setup/dettachobs");
  ros::ServiceClient dettach_kautham_client = n.serviceClient<chesslab_setup::dettachobs>("/chesslab_setup/dettachobs");
  ros::service::waitForService("/link_attacher_node/detach");
  ros::ServiceClient dettach_gazebo_client = n.serviceClient<gazebo_ros_link_attacher::Attach>("/link_attacher_node/detach");
  ros::service::waitForService("/chesslab_setup/setrobconf");
  setrobconf_client = n.serviceClient<chesslab_setup::setrobconf>("/chesslab_setup/setrobconf");

  // Advertise services
  ros::ServiceServer srv_set_robot = n.advertiseService("set_robot",&setRobot);
  ros::ServiceServer srv_joint_lin_traj = n.advertiseService("joint_lin_traj",&jointLinTraj);
  ros::ServiceServer srv_carte_lin_traj = n.advertiseService("carte_lin_traj",&carteLinTraj);
  ros::ServiceServer srv_set_tolerance = n.advertiseService("set_tolerance",&setTolerance);
  ros::ServiceServer srv_call_action = n.advertiseService("call_action",&callAction);
  ros::ServiceServer srv_check_mov_action = n.advertiseService("check_mov_action",&checkMovAction);

  // Subscribe to joint_states
  ros::Subscriber sub_joint_states_team_A = n.subscribe("team_A_arm/joint_states", 1000, &jointStateReceivedTeamA);
  ros::Subscriber sub_joint_states_team_B = n.subscribe("team_B_arm/joint_states", 1000, &jointStateReceivedTeamB);

  current_state_A.position = home_pos;
  current_state_B.position = home_pos;

  // Loop rate to check initially for services (afterwards the waitForResult
  // blocks the loop execution until it's finished)
  ros::Rate rate(10);

  while(ros::ok()){

    if(start){
      if (dettach_required) {
        if (!serviceAttachDettach(dettach_kautham_client, dettach_gazebo_client, "dettach")) {
          ROS_ERROR("Could not dettach object correctly...");
          return -1;
        }
        dettach_required = false;
      }
      moveRobotTrajectory();
      start = false;
    }
    if (attach_required && finished) {
      if (!serviceAttachDettach(attach_kautham_client, attach_gazebo_client, "attach")) {
        ROS_ERROR("Could not attach object correctly...");
        return -1;
      }
      attach_required = false;
    }
		ros::spinOnce();
		rate.sleep();
	}

}

chesslab_setup::dettachobs dettachobs_srv;
