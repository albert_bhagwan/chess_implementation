#include <iostream>
#include <ros/ros.h>
#include <chess_implementation/PlanChessAction.h>
#include <boost/algorithm/string.hpp>
#include <geometry_msgs/Pose.h>
#include <chess_implementation/RobotConfig.h>


using namespace std;

double offset_z = 0.17;
double safety_z = 0.145;
double grasp_z = 0.02;
double gripperOpen = 0.5;
double gripperClosed = 0.54;

// Throwaway locations (world frame) for each piece
map<string, vector<double>> piece_killed_pos = { {"pawnB1",   {-0.13, -0.27}},
                                                 {"pawnB2",   {-0.13, -0.33}},
                                                 {"pawnB3",   {-0.19, -0.27}},
                                                 {"pawnB4",   {-0.19, -0.33}},
                                                 {"pawnB5",   {-0.19, -0.39}},
                                                 {"pawnB6",   {-0.25, -0.27}},
                                                 {"pawnB7",   {-0.25, -0.33}},
                                                 {"pawnB8",   {-0.25, -0.39}},
                                                 {"towerB1",  {-0.31, -0.39}}, //Rook
                                                 {"towerB2",  {-0.37, -0.39}}, //Rook
                                                 {"horseB1",  {-0.31, -0.33}}, //Knight
                                                 {"horseB2",  {-0.37, -0.33}}, //Knight
                                                 {"knightB1", {-0.31, -0.27}}, //Bishop
                                                 {"knightB2", {-0.37, -0.27}}, //Bishop
                                                 {"queenB",   {-0.43, -0.33}},
                                                 {"kingB",    {-0.43, -0.27}}, //=Game Over
                                                 {"pawnW1",   { 0.13,  0.27}},
                                                 {"pawnW2",   { 0.13,  0.33}},
                                                 {"pawnW3",   { 0.19,  0.27}},
                                                 {"pawnW4",   { 0.19,  0.33}},
                                                 {"pawnW5",   { 0.19,  0.39}},
                                                 {"pawnW6",   { 0.25,  0.27}},
                                                 {"pawnW7",   { 0.25,  0.33}},
                                                 {"pawnW8",   { 0.25,  0.39}},
                                                 {"towerW1",  { 0.31,  0.39}}, //Rook
                                                 {"towerW2",  { 0.37,  0.39}}, //Rook
                                                 {"horseW1",  { 0.31,  0.33}}, //Knight
                                                 {"horseW2",  { 0.37,  0.33}}, //Knight
                                                 {"knightW1", { 0.31,  0.27}}, //Bishop
                                                 {"knightW2", { 0.37,  0.27}}, //Bishop
                                                 {"queenW",   { 0.43,  0.33}},
                                                 {"kingW",    { 0.43,  0.27}} }; //=Game Over

vector<string> parseAction(string action){

  // Splitting ACTION, PIECE, PLACE
  vector<string> elements;
  boost::split(elements, action, [](char c){return c == ' ';});
  elements.resize(4);

  // Get piece type (COLOR_TYPE_NUMBER)
  vector<string> piece_split;
  boost::split(piece_split, elements[1], [](char c){return c == '_';});

  // Get piece model name (e.g. "pawnB2")
  string model_name = boost::algorithm::to_lower_copy(piece_split[1]);
  model_name = model_name + piece_split[0].substr(0,1);
  if (piece_split.size()==3) {
    model_name = model_name + piece_split[2];
  }

  // First and third elements are action and place
  // If throwaway, third element is blank
  // Second element = Only piece type (discard color and number)
  elements[1] = piece_split[1];

  // Fourth element = piece in model name style
  elements[3] = model_name;

  return elements;
}

geometry_msgs::Pose getCoordinates(string pieceType, string objCell) {

  vector<string> board_letters = {"A", "B", "C", "D", "E", "F", "G", "H"};

  int cellRow = stoi(objCell.substr(1));
  string cellColStr = objCell.substr(0,1);
  vector<string>::iterator it = find(board_letters.begin(), board_letters.end(), cellColStr);
  int cellCol = distance(board_letters.begin(), it) + 1;

  geometry_msgs::Pose coords;

  // Convert Rows and Cols (1-8) to x,y
  coords.position.x = 0.05*(cellRow-4) - 0.025;
  coords.position.y = -0.05*(cellCol-4) + 0.025;

  // Height of tool0 frame: piece height - pick height + offset of 17cm
  if (pieceType=="PAWN") {
    coords.position.z = 0.04 - (grasp_z - 0*0.005) + offset_z;
  }
  else if (pieceType=="ROOK" || pieceType=="KNIGHT" || pieceType=="BISHOP" ||
           pieceType=="TOWER" || pieceType=="HORSE") {
    coords.position.z = 0.06 - (grasp_z + 0*0.005) + offset_z;
  }
  else {
    coords.position.z = 0.08 - (grasp_z + 0*0.015) + offset_z;
  }

  // Fixed gripper orientation
  coords.orientation.x = 1;
  coords.orientation.y = 0;
  coords.orientation.z = 0;
  coords.orientation.w = 0;

  return coords;
}


bool PlanChessActionCallback(
  chess_implementation::PlanChessAction::Request &req,
  chess_implementation::PlanChessAction::Response &resp){

  ROS_INFO_STREAM("Planning the chess action to execute...");

  vector<string> parse_result = parseAction(req.action);

  if (parse_result[0] == "PICKUP") {
    geometry_msgs::Pose coords = getCoordinates(parse_result[1], parse_result[2]);

    // Messages: vector of RobotConfig -> tcp (Pose), gripper (float)
    chess_implementation::RobotConfig partial_goal;

    // 1) With safety z, go to x,y
    partial_goal.tcp = coords;
    partial_goal.tcp.position.z = safety_z + offset_z;
    partial_goal.gripper = gripperOpen;
    resp.partial_goals.push_back(partial_goal);

    // 2) Go down to x,y,z
    partial_goal.tcp = coords;
    partial_goal.gripper = gripperOpen;
    resp.partial_goals.push_back(partial_goal);

    // 3) Close gripper
    partial_goal.tcp = coords;
    partial_goal.gripper = gripperClosed;
    resp.partial_goals.push_back(partial_goal);

    // 4) Go up to safety z
    partial_goal.tcp = coords;
    partial_goal.tcp.position.z = safety_z + offset_z;
    partial_goal.gripper = gripperClosed;
    resp.partial_goals.push_back(partial_goal);
  }

  else if (parse_result[0] == "PUTDOWN") {
    geometry_msgs::Pose coords = getCoordinates(parse_result[1], parse_result[2]);

    // Messages: vector of RobotConfig -> tcp (Pose), gripper (float)
    chess_implementation::RobotConfig partial_goal;

    // 1) With safety z, go to x,y
    partial_goal.tcp = coords;
    partial_goal.tcp.position.z = safety_z + offset_z;
    partial_goal.gripper = gripperClosed;
    resp.partial_goals.push_back(partial_goal);

    // 2) Go down to x,y,z (split in two steps)
    partial_goal.tcp = coords;
    partial_goal.tcp.position.z = coords.position.z + 0.01; // To avoid collisions with chessboard + 0.01
    partial_goal.gripper = gripperClosed;
    resp.partial_goals.push_back(partial_goal);

    partial_goal.tcp = coords;
    partial_goal.tcp.position.z = coords.position.z;
    partial_goal.gripper = gripperClosed;
    resp.partial_goals.push_back(partial_goal);

    // 3) Open gripper
    partial_goal.tcp = coords;
    partial_goal.tcp.position.z = coords.position.z;
    partial_goal.gripper = gripperOpen;
    resp.partial_goals.push_back(partial_goal);

    // 4) Go up to safety z
    partial_goal.tcp = coords;
    partial_goal.tcp.position.z = safety_z + offset_z;
    partial_goal.gripper = gripperOpen;
    resp.partial_goals.push_back(partial_goal);
  }

  else if (parse_result[0]=="THROWAWAY") {
    // Dummy call to get height and quaternion
    geometry_msgs::Pose coords = getCoordinates(parse_result[1], "E4");

    // Drop-map: simple LUT piece->position
    vector<double> throw_position = piece_killed_pos[parse_result[3]];
    coords.position.x = throw_position[0];
    coords.position.y = throw_position[1];

    // Messages: vector of RobotConfig -> tcp (Pose), gripper (float)
    chess_implementation::RobotConfig partial_goal;

    // 1) With safety z, go to x,y
    partial_goal.tcp = coords;
    partial_goal.tcp.position.z = safety_z + offset_z;
    partial_goal.gripper = gripperClosed;
    resp.partial_goals.push_back(partial_goal);

    // 2) Go down to x,y,z (no board, floor is lower)
    partial_goal.tcp = coords;
    partial_goal.tcp.position.z = coords.position.z - 0.04;
    partial_goal.gripper = gripperClosed;
    resp.partial_goals.push_back(partial_goal);

    // 3) Open gripper
    partial_goal.tcp = coords;
    partial_goal.tcp.position.z = coords.position.z - 0.04;
    partial_goal.gripper = gripperOpen;
    resp.partial_goals.push_back(partial_goal);

    // 4) Go up to safety z
    partial_goal.tcp = coords;
    partial_goal.tcp.position.z = safety_z + offset_z;
    partial_goal.gripper = gripperOpen;
    resp.partial_goals.push_back(partial_goal);
  }

  // For any action, return piece type in model form (e.g. "pawnB2")
  resp.chess_piece = parse_result[3];

  return true;
}


int main(int argc, char** argv){
  ros::init(argc, argv, "chess_planning");
  ros::NodeHandle n;

  ros::ServiceServer srv_plan_chess_action = n.advertiseService("plan_chess_action",&PlanChessActionCallback);

  ros::Rate rate(10);
  while (n.ok()){

    // SLEEP AND SPIN AGAIN
    rate.sleep();
    ros::spinOnce();

  }

}
