#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <string>
#include <geometry_msgs/PoseStamped.h>
#include <chess_implementation/Rectify.h>
#include <chess_implementation/CheckInitialPositions.h>
#include <chess_implementation/ChessPieceInfo.h>
#include <chess_implementation/CheckFreeCells.h>
#include <chess_implementation/CheckContentCell.h>
#include <chess_implementation/GetFENGameState.h>
// #include "std_msgs/Bool.h"

using namespace std;

typedef struct
{
  double x; // current x position of chess piece
  double y; // current y position of chess piece
  string cell; // current cell of chess piece
  ros::Time latest_info; // last time info was received
  vector<string> desired_initial_cells; // location of possible valid initial cells
} chess_piece_info;

map<string, int> chess_board;

map<int, chess_piece_info> chess_pieces = { {201, chess_piece_info{} },
                                            {202, chess_piece_info{} },
                                            {203, chess_piece_info{} },
                                            {204, chess_piece_info{} },
                                            {205, chess_piece_info{} },
                                            {206, chess_piece_info{} },
                                            {207, chess_piece_info{} },
                                            {208, chess_piece_info{} },
                                            {209, chess_piece_info{} },
                                            {210, chess_piece_info{} },
                                            {211, chess_piece_info{} },
                                            {212, chess_piece_info{} },
                                            {213, chess_piece_info{} },
                                            {214, chess_piece_info{} },
                                            {215, chess_piece_info{} },
                                            {216, chess_piece_info{} },
                                            {301, chess_piece_info{} },
                                            {302, chess_piece_info{} },
                                            {303, chess_piece_info{} },
                                            {304, chess_piece_info{} },
                                            {305, chess_piece_info{} },
                                            {306, chess_piece_info{} },
                                            {307, chess_piece_info{} },
                                            {308, chess_piece_info{} },
                                            {309, chess_piece_info{} },
                                            {310, chess_piece_info{} },
                                            {311, chess_piece_info{} },
                                            {312, chess_piece_info{} },
                                            {313, chess_piece_info{} },
                                            {314, chess_piece_info{} },
                                            {315, chess_piece_info{} },
                                            {316, chess_piece_info{} } };

map<int, string> piece_FEN_code = { {201, "p"},
                                    {202, "p"},
                                    {203, "p"},
                                    {204, "p"},
                                    {205, "p"},
                                    {206, "p"},
                                    {207, "p"},
                                    {208, "p"},
                                    {209, "r"},
                                    {210, "r"},
                                    {211, "n"},
                                    {212, "n"},
                                    {213, "b"},
                                    {214, "b"},
                                    {215, "q"},
                                    {216, "k"},
                                    {301, "P"},
                                    {302, "P"},
                                    {303, "P"},
                                    {304, "P"},
                                    {305, "P"},
                                    {306, "P"},
                                    {307, "P"},
                                    {308, "P"},
                                    {309, "R"},
                                    {310, "R"},
                                    {311, "N"},
                                    {312, "N"},
                                    {313, "B"},
                                    {314, "B"},
                                    {315, "Q"},
                                    {316, "K"} };

vector<string> board_letters = {"A", "B", "C", "D", "E", "F", "G", "H"};

string point2cell(double x, double y){

  int row = 4 + ceil(x/0.05);
  int col = 4 - floor(y/0.05);

  string cell;

  if ( row > 8 || row < 1 || col > 8 || col < 1){
    cell = "OUT"; // cell is not in the board
  }
  else{
    cell = board_letters[col-1] + to_string(row);
  }

  return cell;
}

void fillDesiredInitialCells(){

  // BLACK PAWNS
  chess_pieces[201].desired_initial_cells = {"A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7"};
  chess_pieces[202].desired_initial_cells = {"A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7"};
  chess_pieces[203].desired_initial_cells = {"A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7"};
  chess_pieces[204].desired_initial_cells = {"A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7"};
  chess_pieces[205].desired_initial_cells = {"A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7"};
  chess_pieces[206].desired_initial_cells = {"A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7"};
  chess_pieces[207].desired_initial_cells = {"A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7"};
  chess_pieces[208].desired_initial_cells = {"A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7"};

  // BLACK TOWERS
  chess_pieces[209].desired_initial_cells = {"A8", "H8"};
  chess_pieces[210].desired_initial_cells = {"A8", "H8"};

  // BLACK KNIGHTS
  chess_pieces[211].desired_initial_cells = {"B8", "G8"};
  chess_pieces[212].desired_initial_cells = {"B8", "G8"};

  // BLACK BISHOPS
  chess_pieces[213].desired_initial_cells = {"C8", "F8"};
  chess_pieces[214].desired_initial_cells = {"C8", "F8"};

  // BLACK QUEEN
  chess_pieces[215].desired_initial_cells = {"D8"};

  // BLACK KING
  chess_pieces[216].desired_initial_cells = {"E8"};

  // WHITE PAWNS
  chess_pieces[301].desired_initial_cells = {"A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2"};
  chess_pieces[302].desired_initial_cells = {"A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2"};
  chess_pieces[303].desired_initial_cells = {"A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2"};
  chess_pieces[304].desired_initial_cells = {"A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2"};
  chess_pieces[305].desired_initial_cells = {"A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2"};
  chess_pieces[306].desired_initial_cells = {"A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2"};
  chess_pieces[307].desired_initial_cells = {"A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2"};
  chess_pieces[308].desired_initial_cells = {"A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2"};

  // WHITE TOWERS
  chess_pieces[309].desired_initial_cells = {"A1", "H1"};
  chess_pieces[310].desired_initial_cells = {"A1", "H1"};

  // WHITE KNIGHTS
  chess_pieces[311].desired_initial_cells = {"B1", "G1"};
  chess_pieces[312].desired_initial_cells = {"B1", "G1"};

  // WHITE BISHOPS
  chess_pieces[313].desired_initial_cells = {"C1", "F1"};
  chess_pieces[314].desired_initial_cells = {"C1", "F1"};

  // WHITE QUEEN
  chess_pieces[315].desired_initial_cells = {"D1"};

  // WHITE KING
  chess_pieces[316].desired_initial_cells = {"E1"};

}

void setPiecesOut(ros::Duration timeout){

  // Auxiliary variable to indicate aruco ID
  int id;

  // To save current time
  ros::Time current_time = ros::Time::now();

  id = 201;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 202;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 203;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 204;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 205;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 206;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 207;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 208;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 209;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 210;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 211;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 212;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 213;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 214;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 215;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 216;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 301;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 302;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 303;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 304;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 305;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 306;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 307;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 308;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 309;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 310;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 311;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 312;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 313;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 314;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 315;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
  id = 316;
  if (current_time - chess_pieces[id].latest_info > timeout){
    chess_pieces[id].cell = "OUT";
  }
}

void printPiecesCells(){
  ROS_WARN_STREAM(" - CELLS OF CHESS PIECES - ");

  ROS_WARN_STREAM("Black Pawn Cells: " << chess_pieces[201].cell << ", "
     << chess_pieces[202].cell << ", " << chess_pieces[203].cell << ", "
     << chess_pieces[204].cell << ", " << chess_pieces[205].cell << ", "
     << chess_pieces[206].cell << ", " << chess_pieces[207].cell << ", "
     << chess_pieces[208].cell);

  ROS_WARN_STREAM("Black Rook Cells: " << chess_pieces[209].cell
                               << ", " << chess_pieces[210].cell);

  ROS_WARN_STREAM("Black Knight Cells: " << chess_pieces[211].cell
                                 << ", " << chess_pieces[212].cell);

  ROS_WARN_STREAM("Black Bishop Cells: " << chess_pieces[213].cell
                                 << ", " << chess_pieces[214].cell);

  ROS_WARN_STREAM("Black Queen Cell: " << chess_pieces[215].cell);
  ROS_WARN_STREAM("Black King Cell: " << chess_pieces[216].cell);

  ROS_WARN_STREAM("White Pawn Cells: " << chess_pieces[301].cell << ", "
     << chess_pieces[302].cell << ", " << chess_pieces[303].cell << ", "
     << chess_pieces[304].cell << ", " << chess_pieces[305].cell << ", "
     << chess_pieces[306].cell << ", " << chess_pieces[307].cell << ", "
     << chess_pieces[308].cell);

  ROS_WARN_STREAM("White Rook Cells: " << chess_pieces[309].cell
                               << ", " << chess_pieces[310].cell);

  ROS_WARN_STREAM("White Knight Cells: " << chess_pieces[311].cell
                                 << ", " << chess_pieces[312].cell);

  ROS_WARN_STREAM("White Bishop Cells: " << chess_pieces[313].cell
                                 << ", " << chess_pieces[314].cell);

  ROS_WARN_STREAM("White Queen Cell: " << chess_pieces[315].cell);
  ROS_WARN_STREAM("White King Cell: " << chess_pieces[316].cell);
}

// CallBack functions for advertised services
bool CheckInitialPositionsCallback(
  chess_implementation::CheckInitialPositions::Request &req,
  chess_implementation::CheckInitialPositions::Response &resp){

  ROS_INFO_STREAM("Checking if initial positions of chess pieces are ok...");

  // Object of type Rectify (includes ID of incorrect chess piece and  its current position)
  chess_implementation::Rectify obj;

  for (auto i : chess_pieces) {
    vector<string>::iterator it = find(i.second.desired_initial_cells.begin(),
                    i.second.desired_initial_cells.end(), i.second.cell);

    if (it == i.second.desired_initial_cells.end()){
      obj.ID = i.first;
      obj.current_location = i.second.cell;
      resp.error_msg.push_back(obj);
    }
  }

  if (resp.error_msg.size() > 0) {
    resp.ok = false;
  }
  else{
    resp.ok = true;
  }

  return true;
}

bool ChessPieceInfoCallback(
  chess_implementation::ChessPieceInfo::Request &req,
  chess_implementation::ChessPieceInfo::Response &resp){

  ROS_INFO_STREAM("Providing chess piece info of piece: " << req.ID);

  resp.x = chess_pieces[req.ID].x;
  resp.y = chess_pieces[req.ID].y;
  resp.cell = chess_pieces[req.ID].cell;

  return true;
}

bool CheckFreeCellsCallback(
  chess_implementation::CheckFreeCells::Request &req,
  chess_implementation::CheckFreeCells::Response &resp){

  ROS_INFO_STREAM("Checking if given chess cells are free...");

  resp.all_free = true;

  for (unsigned int i = 0; i < req.cells.size(); i++) {
    if ( chess_board.find(req.cells[i]) == chess_board.end() ) {
      continue;
    } else {
      resp.all_free = false;
      break;
    }
  }

  return true;
}

bool CheckContentCellCallback(
  chess_implementation::CheckContentCell::Request &req,
  chess_implementation::CheckContentCell::Response &resp){

  ROS_INFO_STREAM("Checking the content of the given chess cell...");

  if ( chess_board.find(req.cell) == chess_board.end() ) {
    resp.chess_piece = 0;
  } else {
    resp.chess_piece = chess_board[req.cell];
  }

  return true;
}

bool GetFENGameStateCallback(
  chess_implementation::GetFENGameState::Request &req,
  chess_implementation::GetFENGameState::Response &resp){

  ROS_INFO_STREAM("Checking all cells to give FEN game state...");

  // FEN Starts on A8, goes right and down
  // Pieces are lowercase for black, uppercase for white
  // Code: Pawn=P, Rook=R, Knight=N, Bishop=B, Queen=Q, King=K
  // Consecutive empty cells are added and represented with a number
  // Rows are separated with a slash "/"
  // FEN includes who moves next, castling conditions, etc
  // But this cannot be sensed, so only the stated is returned

  int cons_empty_cells;
  string FEN_game_state = "";

  for (int rowi = 8; rowi > 0; rowi--) {

    cons_empty_cells = 0;
    for (int coli = 0; coli < 8; coli++) {

      string current_cell = board_letters[coli] + to_string(rowi);

      if ( chess_board.find(current_cell) == chess_board.end() ) {
        // Empty cell
        cons_empty_cells += 1;
      }
      else {
        // Cell with piece

        // First, deal with previous empty ones
        if (cons_empty_cells>0) {
          FEN_game_state += to_string(cons_empty_cells);
          cons_empty_cells = 0;
        }

        // Add code of found piece
        FEN_game_state += piece_FEN_code[chess_board[current_cell]];
      }
    }
    // End of row

    // First, deal with remaining empty cells
    if (cons_empty_cells>0) {
      FEN_game_state += to_string(cons_empty_cells);
      cons_empty_cells = 0;
    }

    // Add separator
    if (rowi>1) {
      FEN_game_state += "/";
    }

  }

  // Initial FEN game state: "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR"
  // Full FEN record: "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
  resp.FEN_game_state = FEN_game_state;

  return true;
}

// CallBack functions for Aruco Pose subscriptions of left camera
void arucoCallback201Left(geometry_msgs::PoseStamped msg){
  int id = 201; // Black Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback202Left(geometry_msgs::PoseStamped msg){
  int id = 202; // Black Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback203Left(geometry_msgs::PoseStamped msg){
  int id = 203; // Black Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback204Left(geometry_msgs::PoseStamped msg){
  int id = 204; // Black Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback205Left(geometry_msgs::PoseStamped msg){
  int id = 205; // Black Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback206Left(geometry_msgs::PoseStamped msg){
  int id = 206; // Black Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback207Left(geometry_msgs::PoseStamped msg){
  int id = 207; // Black Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback208Left(geometry_msgs::PoseStamped msg){
  int id = 208; // Black Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback209Left(geometry_msgs::PoseStamped msg){
  int id = 209; // Black Rook

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback210Left(geometry_msgs::PoseStamped msg){
  int id = 210; // Black Rook

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback211Left(geometry_msgs::PoseStamped msg){
  int id = 211; // Black Knight

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback212Left(geometry_msgs::PoseStamped msg){
  int id = 212; // Black Knight

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback213Left(geometry_msgs::PoseStamped msg){
  int id = 213; // Black Bishop

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback214Left(geometry_msgs::PoseStamped msg){
  int id = 214; // Black Bishop

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback215Left(geometry_msgs::PoseStamped msg){
  int id = 215; // Black Queen

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback216Left(geometry_msgs::PoseStamped msg){
  int id = 216; // Black King

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}

void arucoCallback301Left(geometry_msgs::PoseStamped msg){
  int id = 301; // White Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback302Left(geometry_msgs::PoseStamped msg){
  int id = 302; // White Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback303Left(geometry_msgs::PoseStamped msg){
  int id = 303; // White Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback304Left(geometry_msgs::PoseStamped msg){
  int id = 304; // White Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback305Left(geometry_msgs::PoseStamped msg){
  int id = 305; // White Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback306Left(geometry_msgs::PoseStamped msg){
  int id = 306; // White Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback307Left(geometry_msgs::PoseStamped msg){
  int id = 307; // White Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback308Left(geometry_msgs::PoseStamped msg){
  int id = 308; // White Pawn

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback309Left(geometry_msgs::PoseStamped msg){
  int id = 309; // White Rook

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback310Left(geometry_msgs::PoseStamped msg){
  int id = 310; // White Rook

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback311Left(geometry_msgs::PoseStamped msg){
  int id = 311; // White Knight

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback312Left(geometry_msgs::PoseStamped msg){
  int id = 312; // White Knight

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback313Left(geometry_msgs::PoseStamped msg){
  int id = 313; // White Bishop

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback314Left(geometry_msgs::PoseStamped msg){
  int id = 314; // White Bishop

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback315Left(geometry_msgs::PoseStamped msg){
  int id = 315; // White Queen

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback316Left(geometry_msgs::PoseStamped msg){
  int id = 316; // White King

  if (msg.pose.position.y < 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}

// CallBack functions for Aruco Pose subscriptions of right camera
void arucoCallback201Right(geometry_msgs::PoseStamped msg){
  int id = 201; // Black Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback202Right(geometry_msgs::PoseStamped msg){
  int id = 202; // Black Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback203Right(geometry_msgs::PoseStamped msg){
  int id = 203; // Black Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback204Right(geometry_msgs::PoseStamped msg){
  int id = 204; // Black Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback205Right(geometry_msgs::PoseStamped msg){
  int id = 205; // Black Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback206Right(geometry_msgs::PoseStamped msg){
  int id = 206; // Black Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback207Right(geometry_msgs::PoseStamped msg){
  int id = 207; // Black Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback208Right(geometry_msgs::PoseStamped msg){
  int id = 208; // Black Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback209Right(geometry_msgs::PoseStamped msg){
  int id = 209; // Black Rook

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback210Right(geometry_msgs::PoseStamped msg){
  int id = 210; // Black Rook

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback211Right(geometry_msgs::PoseStamped msg){
  int id = 211; // Black Knight

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback212Right(geometry_msgs::PoseStamped msg){
  int id = 212; // Black Knight

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback213Right(geometry_msgs::PoseStamped msg){
  int id = 213; // Black Bishop

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback214Right(geometry_msgs::PoseStamped msg){
  int id = 214; // Black Bishop

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback215Right(geometry_msgs::PoseStamped msg){
  int id = 215; // Black Queen

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback216Right(geometry_msgs::PoseStamped msg){
  int id = 216; // Black King

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}

void arucoCallback301Right(geometry_msgs::PoseStamped msg){
  int id = 301; // White Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback302Right(geometry_msgs::PoseStamped msg){
  int id = 302; // White Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback303Right(geometry_msgs::PoseStamped msg){
  int id = 303; // White Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback304Right(geometry_msgs::PoseStamped msg){
  int id = 304; // White Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback305Right(geometry_msgs::PoseStamped msg){
  int id = 305; // White Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback306Right(geometry_msgs::PoseStamped msg){
  int id = 306; // White Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback307Right(geometry_msgs::PoseStamped msg){
  int id = 307; // White Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback308Right(geometry_msgs::PoseStamped msg){
  int id = 308; // White Pawn

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback309Right(geometry_msgs::PoseStamped msg){
  int id = 309; // White Rook

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback310Right(geometry_msgs::PoseStamped msg){
  int id = 310; // White Rook

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback311Right(geometry_msgs::PoseStamped msg){
  int id = 311; // White Knight

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback312Right(geometry_msgs::PoseStamped msg){
  int id = 312; // White Knight

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback313Right(geometry_msgs::PoseStamped msg){
  int id = 313; // White Bishop

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback314Right(geometry_msgs::PoseStamped msg){
  int id = 314; // White Bishop

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback315Right(geometry_msgs::PoseStamped msg){
  int id = 315; // White Queen

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}
void arucoCallback316Right(geometry_msgs::PoseStamped msg){
  int id = 316; // White King

  if (msg.pose.position.y > 0){
    chess_pieces[id].x = msg.pose.position.x;
    chess_pieces[id].y = msg.pose.position.y;
    chess_pieces[id].cell = point2cell(msg.pose.position.x, msg.pose.position.y);
    chess_pieces[id].latest_info = ros::Time::now();
  }
}

int main(int argc, char** argv){

  ros::init(argc, argv, "chess_sensing");

  ros::NodeHandle n;

  // Subscribe to all the Aruco pose messages:
  // 1. LEFT SIDE
  //  a) Black pieces
  ros::Subscriber sub_201_left = n.subscribe("/camera_left/aruco_single_201/pose", 1000, arucoCallback201Left);
  ros::Subscriber sub_202_left = n.subscribe("/camera_left/aruco_single_202/pose", 1000, arucoCallback202Left);
  ros::Subscriber sub_203_left = n.subscribe("/camera_left/aruco_single_203/pose", 1000, arucoCallback203Left);
  ros::Subscriber sub_204_left = n.subscribe("/camera_left/aruco_single_204/pose", 1000, arucoCallback204Left);
  ros::Subscriber sub_205_left = n.subscribe("/camera_left/aruco_single_205/pose", 1000, arucoCallback205Left);
  ros::Subscriber sub_206_left = n.subscribe("/camera_left/aruco_single_206/pose", 1000, arucoCallback206Left);
  ros::Subscriber sub_207_left = n.subscribe("/camera_left/aruco_single_207/pose", 1000, arucoCallback207Left);
  ros::Subscriber sub_208_left = n.subscribe("/camera_left/aruco_single_208/pose", 1000, arucoCallback208Left);
  ros::Subscriber sub_209_left = n.subscribe("/camera_left/aruco_single_209/pose", 1000, arucoCallback209Left);
  ros::Subscriber sub_210_left = n.subscribe("/camera_left/aruco_single_210/pose", 1000, arucoCallback210Left);
  ros::Subscriber sub_211_left = n.subscribe("/camera_left/aruco_single_211/pose", 1000, arucoCallback211Left);
  ros::Subscriber sub_212_left = n.subscribe("/camera_left/aruco_single_212/pose", 1000, arucoCallback212Left);
  ros::Subscriber sub_213_left = n.subscribe("/camera_left/aruco_single_213/pose", 1000, arucoCallback213Left);
  ros::Subscriber sub_214_left = n.subscribe("/camera_left/aruco_single_214/pose", 1000, arucoCallback214Left);
  ros::Subscriber sub_215_left = n.subscribe("/camera_left/aruco_single_215/pose", 1000, arucoCallback215Left);
  ros::Subscriber sub_216_left = n.subscribe("/camera_left/aruco_single_216/pose", 1000, arucoCallback216Left);
  //  b) White pieces
  ros::Subscriber sub_301_left = n.subscribe("/camera_left/aruco_single_301/pose", 1000, arucoCallback301Left);
  ros::Subscriber sub_302_left = n.subscribe("/camera_left/aruco_single_302/pose", 1000, arucoCallback302Left);
  ros::Subscriber sub_303_left = n.subscribe("/camera_left/aruco_single_303/pose", 1000, arucoCallback303Left);
  ros::Subscriber sub_304_left = n.subscribe("/camera_left/aruco_single_304/pose", 1000, arucoCallback304Left);
  ros::Subscriber sub_305_left = n.subscribe("/camera_left/aruco_single_305/pose", 1000, arucoCallback305Left);
  ros::Subscriber sub_306_left = n.subscribe("/camera_left/aruco_single_306/pose", 1000, arucoCallback306Left);
  ros::Subscriber sub_307_left = n.subscribe("/camera_left/aruco_single_307/pose", 1000, arucoCallback307Left);
  ros::Subscriber sub_308_left = n.subscribe("/camera_left/aruco_single_308/pose", 1000, arucoCallback308Left);
  ros::Subscriber sub_309_left = n.subscribe("/camera_left/aruco_single_309/pose", 1000, arucoCallback309Left);
  ros::Subscriber sub_310_left = n.subscribe("/camera_left/aruco_single_310/pose", 1000, arucoCallback310Left);
  ros::Subscriber sub_311_left = n.subscribe("/camera_left/aruco_single_311/pose", 1000, arucoCallback311Left);
  ros::Subscriber sub_312_left = n.subscribe("/camera_left/aruco_single_312/pose", 1000, arucoCallback312Left);
  ros::Subscriber sub_313_left = n.subscribe("/camera_left/aruco_single_313/pose", 1000, arucoCallback313Left);
  ros::Subscriber sub_314_left = n.subscribe("/camera_left/aruco_single_314/pose", 1000, arucoCallback314Left);
  ros::Subscriber sub_315_left = n.subscribe("/camera_left/aruco_single_315/pose", 1000, arucoCallback315Left);
  ros::Subscriber sub_316_left = n.subscribe("/camera_left/aruco_single_316/pose", 1000, arucoCallback316Left);

  // 2. RIGHT SIDE
  //  a) Black pieces
  ros::Subscriber sub_201_right = n.subscribe("/camera_right/aruco_single_201/pose", 1000, arucoCallback201Right);
  ros::Subscriber sub_202_right = n.subscribe("/camera_right/aruco_single_202/pose", 1000, arucoCallback202Right);
  ros::Subscriber sub_203_right = n.subscribe("/camera_right/aruco_single_203/pose", 1000, arucoCallback203Right);
  ros::Subscriber sub_204_right = n.subscribe("/camera_right/aruco_single_204/pose", 1000, arucoCallback204Right);
  ros::Subscriber sub_205_right = n.subscribe("/camera_right/aruco_single_205/pose", 1000, arucoCallback205Right);
  ros::Subscriber sub_206_right = n.subscribe("/camera_right/aruco_single_206/pose", 1000, arucoCallback206Right);
  ros::Subscriber sub_207_right = n.subscribe("/camera_right/aruco_single_207/pose", 1000, arucoCallback207Right);
  ros::Subscriber sub_208_right = n.subscribe("/camera_right/aruco_single_208/pose", 1000, arucoCallback208Right);
  ros::Subscriber sub_209_right = n.subscribe("/camera_right/aruco_single_209/pose", 1000, arucoCallback209Right);
  ros::Subscriber sub_210_right = n.subscribe("/camera_right/aruco_single_210/pose", 1000, arucoCallback210Right);
  ros::Subscriber sub_211_right = n.subscribe("/camera_right/aruco_single_211/pose", 1000, arucoCallback211Right);
  ros::Subscriber sub_212_right = n.subscribe("/camera_right/aruco_single_212/pose", 1000, arucoCallback212Right);
  ros::Subscriber sub_213_right = n.subscribe("/camera_right/aruco_single_213/pose", 1000, arucoCallback213Right);
  ros::Subscriber sub_214_right = n.subscribe("/camera_right/aruco_single_214/pose", 1000, arucoCallback214Right);
  ros::Subscriber sub_215_right = n.subscribe("/camera_right/aruco_single_215/pose", 1000, arucoCallback215Right);
  ros::Subscriber sub_216_right = n.subscribe("/camera_right/aruco_single_216/pose", 1000, arucoCallback216Right);
  //  b) White pieces
  ros::Subscriber sub_301_right = n.subscribe("/camera_right/aruco_single_301/pose", 1000, arucoCallback301Right);
  ros::Subscriber sub_302_right = n.subscribe("/camera_right/aruco_single_302/pose", 1000, arucoCallback302Right);
  ros::Subscriber sub_303_right = n.subscribe("/camera_right/aruco_single_303/pose", 1000, arucoCallback303Right);
  ros::Subscriber sub_304_right = n.subscribe("/camera_right/aruco_single_304/pose", 1000, arucoCallback304Right);
  ros::Subscriber sub_305_right = n.subscribe("/camera_right/aruco_single_305/pose", 1000, arucoCallback305Right);
  ros::Subscriber sub_306_right = n.subscribe("/camera_right/aruco_single_306/pose", 1000, arucoCallback306Right);
  ros::Subscriber sub_307_right = n.subscribe("/camera_right/aruco_single_307/pose", 1000, arucoCallback307Right);
  ros::Subscriber sub_308_right = n.subscribe("/camera_right/aruco_single_308/pose", 1000, arucoCallback308Right);
  ros::Subscriber sub_309_right = n.subscribe("/camera_right/aruco_single_309/pose", 1000, arucoCallback309Right);
  ros::Subscriber sub_310_right = n.subscribe("/camera_right/aruco_single_310/pose", 1000, arucoCallback310Right);
  ros::Subscriber sub_311_right = n.subscribe("/camera_right/aruco_single_311/pose", 1000, arucoCallback311Right);
  ros::Subscriber sub_312_right = n.subscribe("/camera_right/aruco_single_312/pose", 1000, arucoCallback312Right);
  ros::Subscriber sub_313_right = n.subscribe("/camera_right/aruco_single_313/pose", 1000, arucoCallback313Right);
  ros::Subscriber sub_314_right = n.subscribe("/camera_right/aruco_single_314/pose", 1000, arucoCallback314Right);
  ros::Subscriber sub_315_right = n.subscribe("/camera_right/aruco_single_315/pose", 1000, arucoCallback315Right);
  ros::Subscriber sub_316_right = n.subscribe("/camera_right/aruco_single_316/pose", 1000, arucoCallback316Right);

  // Fill desired initial cells of the map chess_pieces
  fillDesiredInitialCells();

  // Advertise services
  ros::ServiceServer srv_check_initial_positions = n.advertiseService("check_initial_positions",&CheckInitialPositionsCallback);
  ros::ServiceServer srv_chess_piece_info = n.advertiseService("chess_piece_info",&ChessPieceInfoCallback);
  ros::ServiceServer srv_check_free_cells = n.advertiseService("check_free_cells",&CheckFreeCellsCallback);
  ros::ServiceServer srv_check_content_cell = n.advertiseService("check_content_cell",&CheckContentCellCallback);
  ros::ServiceServer srv_get_fen_game_state = n.advertiseService("get_fen_game_state",&GetFENGameStateCallback);

  // If no info received in 0.2 s then assume the chess piece is not in the board
  ros::Duration timeout = ros::Duration(0.2);

  ros::Rate rate(10); // aruco's pose topic is published @30 Hz aprox
  while (n.ok()){

    // CHECK IF PIECES ARE STILL IN THE BOARD
    setPiecesOut(timeout);

    // DEBUG INFORMATION
    //printPiecesCells();

    // CLEAR AND UPDATE CHESS BOARD
    chess_board.clear();

    for (auto i : chess_pieces) {
      //ROS_INFO_STREAM("Chess piece ID: " << i.first << " in cell: " << i.second.cell);
      chess_board[i.second.cell] = i.first;
    }

    // SLEEP AND SPIN AGAIN
    rate.sleep();
    ros::spinOnce();

  }
  return 0;
};
