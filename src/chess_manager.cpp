#include <ros/ros.h>
#include <string>
#include <dynamic_reconfigure/server.h>
#include "geometry_msgs/Quaternion.h"
#include <chess_implementation/Rectify.h>
#include <chess_implementation/RobotConfig.h>
#include <chess_implementation/CheckInitialPositions.h>
#include <chess_implementation/ChessPieceInfo.h>
#include <chess_implementation/CheckFreeCells.h>
#include <chess_implementation/CheckContentCell.h>
#include <chess_implementation/PlanChessAction.h>
#include <chess_implementation/SetTrajectory.h>
#include <chess_implementation/SetTolerance.h>
#include <chess_implementation/CheckMovAction.h>
#include <chess_implementation/SetRobot.h>
#include <chess_implementation/ChessInputConfig.h>
#include <chesslab_setup/ffplan.h>
#include <std_srvs/Empty.h>

using namespace std;

// Chess command structure
typedef struct {
  bool pending;     // Boolean indicating if a command is pending to be served
  bool valid;       // Boolean indicating if the command can be executed
  int chess_piece;  // ID of the chess piece to move
  string goal_cell; // Desired cell of chess piece
} chess_command;

chess_command command_A;
chess_command command_B;

// Valid chessboard columns
vector<string> board_letters = {"A", "B", "C", "D", "E", "F", "G", "H"};

// Name of each piece by Aruco ID (normal names)
map<int, string> chess_piece_type = { {201, "BLACK PAWN"},
                                      {202, "BLACK PAWN"},
                                      {203, "BLACK PAWN"},
                                      {204, "BLACK PAWN"},
                                      {205, "BLACK PAWN"},
                                      {206, "BLACK PAWN"},
                                      {207, "BLACK PAWN"},
                                      {208, "BLACK PAWN"},
                                      {209, "BLACK ROOK"},
                                      {210, "BLACK ROOK"},
                                      {211, "BLACK KNIGHT"},
                                      {212, "BLACK KNIGHT"},
                                      {213, "BLACK BISHOP"},
                                      {214, "BLACK BISHOP"},
                                      {215, "BLACK QUEEN"},
                                      {216, "BLACK KING"},
                                      {301, "WHITE PAWN"},
                                      {302, "WHITE PAWN"},
                                      {303, "WHITE PAWN"},
                                      {304, "WHITE PAWN"},
                                      {305, "WHITE PAWN"},
                                      {306, "WHITE PAWN"},
                                      {307, "WHITE PAWN"},
                                      {308, "WHITE PAWN"},
                                      {309, "WHITE ROOK"},
                                      {310, "WHITE ROOK"},
                                      {311, "WHITE KNIGHT"},
                                      {312, "WHITE KNIGHT"},
                                      {313, "WHITE BISHOP"},
                                      {314, "WHITE BISHOP"},
                                      {315, "WHITE QUEEN"},
                                      {316, "WHITE KING"} };

// Aruco ID corresponding to each piece by model name
map<string, int> piece_model_ID = { {"pawnB1",   201},
                                    {"pawnB2",   202},
                                    {"pawnB3",   203},
                                    {"pawnB4",   204},
                                    {"pawnB5",   205},
                                    {"pawnB6",   206},
                                    {"pawnB7",   207},
                                    {"pawnB8",   208},
                                    {"towerB1",  209}, //Rook
                                    {"towerB2",  210}, //Rook
                                    {"horseB1",  211}, //Knight
                                    {"horseB2",  212}, //Knight
                                    {"knightB1", 213}, //Bishop
                                    {"knightB2", 214}, //Bishop
                                    {"queenB",   215},
                                    {"kingB",    216},
                                    {"pawnW1",   301},
                                    {"pawnW2",   302},
                                    {"pawnW3",   303},
                                    {"pawnW4",   304},
                                    {"pawnW5",   305},
                                    {"pawnW6",   306},
                                    {"pawnW7",   307},
                                    {"pawnW8",   308},
                                    {"towerW1",  309}, //Rook
                                    {"towerW2",  310}, //Rook
                                    {"horseW1",  311}, //Knight
                                    {"horseW2",  312}, //Knight
                                    {"knightW1", 313}, //Bishop
                                    {"knightW2", 314}, //Bishop
                                    {"queenW",   315},
                                    {"kingW",    316} };

// Possible initial cells depending on type
map<string, vector<string>> piece_type_ini_cells = {
  {"BLACK PAWN",   {"A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7"} },
  {"BLACK ROOK",   {"A8", "H8"} },
  {"BLACK KNIGHT", {"B8", "G8"} },
  {"BLACK BISHOP", {"C8", "F8"} },
  {"BLACK QUEEN",  {"D8"} },
  {"BLACK KING",   {"E8"} },
  {"WHITE PAWN",   {"A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2"} },
  {"WHITE ROOK",   {"A1", "H1"} },
  {"WHITE KNIGHT", {"B1", "G1"} },
  {"WHITE BISHOP", {"C1", "F1"} },
  {"WHITE QUEEN",  {"D1"} },
  {"WHITE KING",   {"E1"} }
};

// Castling global variables
map<string, bool> castling_available = {
  {"WHITE KINGSIDE", true},
  {"WHITE QUEENSIDE", true},
  {"BLACK KINGSIDE", true},
  {"BLACK QUEENSIDE", true}
};
int castling_rook;
vector<string> castling_cells;
bool is_castling_move = false;

// Function that returns if the input is a valid chessboard cell
bool isValidCell(string cellCandidate){

  // Check if there are only two characters
  if (cellCandidate.size() != 2) {
    return false;
  }
  else {
    int row;

    try{
      row = stoi(cellCandidate.substr(1));
    }
    catch (exception& e){
      ROS_ERROR_STREAM("Second character of the cell should be and int number!");
      return false;
    }

    if (row<1 || row > 8) {
      return false;
    }

    string col = cellCandidate.substr(0,1);
    vector<string>::iterator it = find(board_letters.begin(), board_letters.end(), col);
    if (it == board_letters.end()){
      return false;
    }

    return true;
  }
}

// Function that given a piece and the origin/goal cells
//   returns if the movement is valid according to the chess rules.
//   Valid origin and goal cells are assumed.
//   Considers diagonal pawn moves and castling as always valid (further checks)
bool isValidChessMove(int pieceID, string originCell, string goalCell) {

  // Reset castling variable
  is_castling_move = false;


  // Origin cannot be outside board (PROMOTIONS ARE NOT CHECKED HERE)
  if (originCell == "OUT") {
    return false;
  }

  // No movement
  if (goalCell == originCell) {
    return false;
  }

  // Get all coordinates as integers
  int originRow = stoi(originCell.substr(1));
  int goalRow = stoi(goalCell.substr(1));

  string originColStr = originCell.substr(0,1);
  vector<string>::iterator itO = find(board_letters.begin(), board_letters.end(), originColStr);
  int originCol = distance(board_letters.begin(), itO);

  string goalColStr = goalCell.substr(0,1);
  vector<string>::iterator itG = find(board_letters.begin(), board_letters.end(), goalColStr);
  int goalCol = distance(board_letters.begin(), itG);

  // Get increments
  int deltaRow = goalRow - originRow;
  int deltaCol = goalCol - originCol;


  // BLACK PAWNS
  if (chess_piece_type[pieceID] == "BLACK PAWN") {
    // Row must decrease 1, or 2 if origin row is 7. Column can change 1 if killing

    // Opening move: can move -2 rows but 0 columns
    if (originRow==7) {
      if (deltaRow==-2 && deltaCol==0) {
        return true;
      }
    }
    // Not checking if kill actually possible (must check later)
    if (deltaRow==-1 && abs(deltaCol)<=1) {
      return true;
    }
    // All real moves explored: must be invalid
    return false;
  }

  // WHITE PAWNS
  if (chess_piece_type[pieceID] == "WHITE PAWN") {
    // Row must increase 1, or 2 if origin row is 2. Column can change 1 if killing

    // Opening move: can move +2 rows but 0 columns
    if (originRow==2) {
      if (deltaRow==2 && deltaCol==0) {
        return true;
      }
    }
    // Not checking if kill actually possible (must check later)
    if (deltaRow==1 && abs(deltaCol)<=1) {
      return true;
    }
    // All real moves explored: must be invalid
    return false;
  }

  // ALL ROOKS
  if (chess_piece_type[pieceID] == "BLACK ROOK" || chess_piece_type[pieceID] == "WHITE ROOK") {
    // With valid cells, one increment must be 0 and the other can take any value (1-8)

    // As OUT is discarded and valid cells are given, delta<8
    // As no movement is already ruled out, second condition is redundant
    if (deltaCol==0 && abs(deltaRow)>0) {
      return true;
    }
    if (deltaRow==0 && abs(deltaCol)>0) {
      return true;
    }

    // All real moves explored: must be invalid
    return false;
  }

  // ALL KNIGHTS
  if (chess_piece_type[pieceID] == "BLACK KNIGHT" || chess_piece_type[pieceID] == "WHITE KNIGHT") {
    // L moves: deltaCol = +/- 2 means deltaRow= +/- 1, and vice versa

    if (abs(deltaCol)==2 && abs(deltaRow)==1) {
      return true;
    }
    if (abs(deltaCol)==1 && abs(deltaRow)==2) {
      return true;
    }

    // All real moves explored: must be invalid
    return false;
  }

  // ALL BISHOPS
  if (chess_piece_type[pieceID] == "BLACK BISHOP" || chess_piece_type[pieceID] == "WHITE BISHOP") {
    // Diagonal moves: deltaCol == +/- deltaRow

    // We assume valid cells. "No movement" has been discarded before
    if (abs(deltaCol) == abs(deltaRow)) {
      return true;
    }

    // All real moves explored: must be invalid
    return false;
  }

  // BOTH QUEENS
  if (chess_piece_type[pieceID] == "BLACK QUEEN" || chess_piece_type[pieceID] == "WHITE QUEEN") {
    // Either straight or diagonal: rook+bishop. We assume valid cells.

    // Rook-like straight lines. Second condition still redundant
    if (deltaCol==0 && abs(deltaRow)>0) {
      return true;
    }
    if (deltaRow==0 && abs(deltaCol)>0) {
      return true;
    }

    // Bishop-like diagonals
    if (abs(deltaCol) == abs(deltaRow)) {
      return true;
    }

    // All real moves explored: must be invalid
    return false;
  }

  // BLACK KING
  if (chess_piece_type[pieceID] == "BLACK KING") {
    // Can move 1 cell on any direction. Kings must be separated to check castling

    // Usual moves
    if (abs(deltaCol)==1 || abs(deltaRow)==1) {
      return true;
    }

    // Castling: on cell E8, move just +/- 2 cols. Needs further checks later
    if (originCell=="E8" && deltaRow==0){
      // Positive deltaCol is Kingside, negative is Queenside
      if (deltaCol == 2) {
        if (castling_available["BLACK KINGSIDE"]) {
          ROS_INFO_STREAM("Kingside Castling of Black King available");
          castling_cells = {"H8", "F8"};
          is_castling_move = true;
          return true;
        }
        else{
          ROS_WARN_STREAM("Kingside Castling of Black King unavailable");
          return false;
        }
      }
      else if (deltaCol == -2) {
        if (castling_available["BLACK QUEENSIDE"]) {
          ROS_INFO_STREAM("Queenside Castling of Black King available");
          castling_cells = {"A8", "D8"};
          is_castling_move = true;
          return true;
        }
        else{
          ROS_WARN_STREAM("Queenside Castling of Black King unavailable");
          return false;
        }
      }
    }

    // All real moves explored: must be invalid
    return false;
  }

  // WHITE KING
  if (chess_piece_type[pieceID] == "WHITE KING") {
    // Can move 1 cell on any direction. Kings must be separated to check castling

    // Usual moves
    if (abs(deltaCol)==1 || abs(deltaRow)==1) {
      return true;
    }

    // Castling: on cell E1, move just +/- 2 cols. Needs further checks later
    if (originCell=="E1" && deltaRow==0) {
      // Positive deltaCol is Kingside, negative is Queenside
      if (deltaCol == 2) {
        if (castling_available["WHITE KINGSIDE"]) {
          ROS_INFO_STREAM("Kingside Castling of White King available");
          castling_cells = {"H1", "F1"};
          is_castling_move = true;
          return true;
        }
        else{
          ROS_WARN_STREAM("Kingside Castling of White King unavailable");
          return false;
        }
      }
      else if (deltaCol == -2) {
        if (castling_available["WHITE QUEENSIDE"]) {
          ROS_INFO_STREAM("Queenside Castling of White King available");
          castling_cells = {"A1", "D1"};
          is_castling_move = true;
          return true;
        }
        else{
          ROS_WARN_STREAM("Queenside Castling of White King unavailable");
          return false;
        }
      }
    }

    // All real moves explored: must be invalid
    return false;
  }

  // Something went wrong if this is reached
  ROS_WARN_STREAM("Error checking move!");
  return false;
}

// Function that obtains all cells between the given origin and goal
//   to check their content, as all pieces except Knights cannot jump over others
vector<string> getPathCells(int pieceID, string originCell, string goalCell) {
  // Valid origin and goal cells are assumed

  // Get all coordinates as integers
  int originRow = stoi(originCell.substr(1));
  int goalRow = stoi(goalCell.substr(1));

  string originColStr = originCell.substr(0,1);
  vector<string>::iterator itO = find(board_letters.begin(), board_letters.end(), originColStr);
  int originCol = distance(board_letters.begin(), itO);

  string goalColStr = goalCell.substr(0,1);
  vector<string>::iterator itG = find(board_letters.begin(), board_letters.end(), goalColStr);
  int goalCol = distance(board_letters.begin(), itG);

  // Get increments
  int deltaRow = goalRow - originRow;
  int deltaCol = goalCol - originCol;

  vector<string> path_cells = {};

  // Knights can jump other pieces, no need to check
  if (chess_piece_type[pieceID] == "BLACK KNIGHT" || chess_piece_type[pieceID] == "WHITE KNIGHT") {
    return path_cells;
  }

  // If movement to adjacent cell, no cells between origin and goal
  if (abs(deltaCol) <= 1 && abs(deltaRow) <= 1) {
    return path_cells;
  }

  // Movement in rows on the same column
  if (deltaCol == 0) {
    for (unsigned int i = 1; i < abs(deltaRow); i++) {
      path_cells.push_back(originColStr + to_string(originRow+((deltaRow>0)?1:-1)*i));
    }
    return path_cells;
  }

  // Movement in columns on the same row
  if (deltaRow == 0) {
    for (unsigned int i = 1; i < abs(deltaCol); i++) {
      path_cells.push_back(board_letters[originCol+((deltaCol>0)?1:-1)*i] + to_string(originRow));
    }
    return path_cells;
  }

  // Movement in diagonal
  if (abs(deltaCol) == abs(deltaRow)) {
    for (unsigned int i = 1; i < abs(deltaCol); i++) {
      path_cells.push_back(board_letters[originCol+((deltaCol>0)?1:-1)*i] + to_string(originRow+((deltaRow>0)?1:-1)*i));
    }
    return path_cells;
  }

  // Something went wrong if this is reached
  ROS_WARN_STREAM("Error checking cells!");
  return path_cells;

}

// Function that loops until all pieces are at correct initial positions to
//   start a chess game, giving feedback of incorrectly placed pieces
bool waitForInitialPositions(ros::ServiceClient clt_check_initial_positions) {

  chess_implementation::CheckInitialPositions::Request ini_req;
  chess_implementation::CheckInitialPositions::Response ini_resp;

  bool success_ini = clt_check_initial_positions.call(ini_req, ini_resp);

  if(success_ini){
      ROS_INFO_STREAM("Checked initial positions");
      while (!ini_resp.ok) {

        stringstream sstr_ini;
        sstr_ini << endl << "The following chess pieces are misplaced:" << endl;

        string ptype_i = " ";
        vector<string> cells_ptype;
        for (unsigned int i = 0; i < ini_resp.error_msg.size(); i++) {

          // New misplaced piece type
          if (chess_piece_type[ini_resp.error_msg[i].ID] != ptype_i) {

            // If coming from another type, tell correct positions
            if (ptype_i != " ") {
              sstr_ini << "  -> They should be in the following cells: ";
              cells_ptype = piece_type_ini_cells[ptype_i];

              for (unsigned int j = 0; j < cells_ptype.size(); j++) {
                sstr_ini << cells_ptype[j];
                if (j<cells_ptype.size()-1) {
                  sstr_ini << ", ";
                }
                else {
                  sstr_ini << "." << endl;
                }
              }
            }

            // Update type and add to message
            ptype_i = chess_piece_type[ini_resp.error_msg[i].ID];
            sstr_ini << "* " << ptype_i << "S:" << endl;
          }

          sstr_ini << "  - " << ini_resp.error_msg[i].ID << ", current location: "
                             << ini_resp.error_msg[i].current_location << endl;
        }

        // Correct cells for final piece type
        sstr_ini << "  -> They should be in the following cells: ";
        cells_ptype = piece_type_ini_cells[ptype_i];

        for (unsigned int j = 0; j < cells_ptype.size(); j++) {
          sstr_ini << cells_ptype[j];
          if (j<cells_ptype.size()-1) {
            sstr_ini << ", ";
          }
          else {
            sstr_ini << "." << endl;
          }
        }

        // Send feedback
        ROS_WARN_STREAM(sstr_ini.str());

        // Retry
        bool success_ini = clt_check_initial_positions.call(ini_req,ini_resp);
      }

      // If corrected positions
      ROS_INFO_STREAM("All pieces are now correctly placed");
      return true;
  }
  else{
      ROS_ERROR_STREAM("Failed to check initial positions");
      return false;
  }

}

// Function to filter pawn normal/kill movements knowing the goal content
bool isPawnMovValid(string originCell, string goalCell, int goalContent){

  // Get all coordinates as integers
  int originRow = stoi(originCell.substr(1));
  int goalRow = stoi(goalCell.substr(1));

  string originColStr = originCell.substr(0,1);
  vector<string>::iterator itO = find(board_letters.begin(), board_letters.end(), originColStr);
  int originCol = distance(board_letters.begin(), itO);

  string goalColStr = goalCell.substr(0,1);
  vector<string>::iterator itG = find(board_letters.begin(), board_letters.end(), goalColStr);
  int goalCol = distance(board_letters.begin(), itG);

  // Get increments
  int deltaRow = goalRow - originRow;
  int deltaCol = goalCol - originCol;

  // If movement is going one or two rows forwards the goal cell should be empty
  if (deltaCol==0) {
    if (goalContent!=0){
      return false;
    }
  }
  else{
    if (goalContent==0){
      return false;
    }
  }
  return true;
}

// Function to call the Trajectory Module and execute the movement
int executeLinTraj(chess_implementation::RobotConfig partial_goal,
                        string piece_to_move, bool return_home,
                        string robot_name,
                        ros::ServiceClient TrajClient,
                        ros::ServiceClient TolClient,
                        ros::ServiceClient CallClient){
  // With a partial goal, calls services to get trajectory and execute it
  // Needs one "set trajectory" service client:
  // - clt_joint_lin_traj (chess_implementation::SetTrajectory) -> TrajClient
  // - clt_carte_lin_traj (chess_implementation::SetTrajectory) -> TrajClient
  // And tolerance & call service clients:
  // - clt_set_tolerance (chess_implementation::SetTolerance) -> TolClient
  // - clt_call_action (std_srvs::Empty) -> CallClient


  // 1) Set trajectory using service
  chess_implementation::SetTrajectory::Request traj_req;
  chess_implementation::SetTrajectory::Response traj_resp;

  // Set final config of this step
  traj_req.final_config = partial_goal;
  traj_req.return_home = return_home;
  traj_req.arucoID = piece_model_ID[piece_to_move];
  traj_req.chess_piece = piece_to_move;

  if (!return_home) {
    ROS_INFO_STREAM("Goal [world frame] -> X: " << partial_goal.tcp.position.x << "; Y: " <<
                      partial_goal.tcp.position.y << "; Z: " << partial_goal.tcp.position.z);
  }
  else {
    ROS_INFO_STREAM("Returning to home configuration");
  }


  // Call the trajectory service
  int success_traj = TrajClient.call(traj_req, traj_resp);

  if(success_traj){
    if (traj_resp.collision_detected) {
      return 1;
    }
    else {
      ROS_INFO_STREAM("Trajectory set correctly");
    }
  }
  else{
      ROS_ERROR_STREAM("Failed to set trajectory");
      return -1;
  }

  // 2) Set tolerances using service
  chess_implementation::SetTolerance::Request tol_req;
  chess_implementation::SetTolerance::Response tol_resp;

  //Set goal tolerances
  tol_req.goal_tol.resize(7);
  tol_req.goal_tol[0].name = robot_name + "_elbow_joint";
  tol_req.goal_tol[1].name = robot_name + "_gripper_right_driver_joint";
  tol_req.goal_tol[2].name = robot_name + "_shoulder_lift_joint";
  tol_req.goal_tol[3].name = robot_name + "_shoulder_pan_joint";
  tol_req.goal_tol[4].name = robot_name + "_wrist_1_joint";
  tol_req.goal_tol[5].name = robot_name + "_wrist_2_joint";
  tol_req.goal_tol[6].name = robot_name + "_wrist_3_joint";

  tol_req.goal_tol[0].position = 0.001;
  tol_req.goal_tol[1].position = 0.001;
  tol_req.goal_tol[2].position = 0.001;
  tol_req.goal_tol[3].position = 0.001;
  tol_req.goal_tol[4].position = 0.001;
  tol_req.goal_tol[5].position = 0.001;
  tol_req.goal_tol[6].position = 0.001;

  tol_req.goal_tol[0].velocity = 0.1;
  tol_req.goal_tol[1].velocity = 0.1;
  tol_req.goal_tol[2].velocity = 0.1;
  tol_req.goal_tol[3].velocity = 0.1;
  tol_req.goal_tol[4].velocity = 0.1;
  tol_req.goal_tol[5].velocity = 0.1;
  tol_req.goal_tol[6].velocity = 0.1;

  // Set path tolerances
  tol_req.path_tol.resize(7);
  tol_req.path_tol[0].name = robot_name + "_elbow_joint";
  tol_req.path_tol[1].name = robot_name + "_gripper_right_driver_joint";
  tol_req.path_tol[2].name = robot_name + "_shoulder_lift_joint";
  tol_req.path_tol[3].name = robot_name + "_shoulder_pan_joint";
  tol_req.path_tol[4].name = robot_name + "_wrist_1_joint";
  tol_req.path_tol[5].name = robot_name + "_wrist_2_joint";
  tol_req.path_tol[6].name = robot_name + "_wrist_3_joint";

  tol_req.path_tol[0].position = 0.01;
  tol_req.path_tol[1].position = 0.01;
  tol_req.path_tol[2].position = 0.01;
  tol_req.path_tol[3].position = 0.01;
  tol_req.path_tol[4].position = 0.01;
  tol_req.path_tol[5].position = 0.01;
  tol_req.path_tol[6].position = 0.01;

  tol_req.path_tol[0].velocity = 100;
  tol_req.path_tol[1].velocity = 100;
  tol_req.path_tol[2].velocity = 100;
  tol_req.path_tol[3].velocity = 100;
  tol_req.path_tol[4].velocity = 100;
  tol_req.path_tol[5].velocity = 100;
  tol_req.path_tol[6].velocity = 100;

  // Set goal time tolerance
  tol_req.goal_time_tol = ros::Duration(10);

  // Call the tolerance service
  bool success_tol = TolClient.call(tol_req, tol_resp);

  if(success_tol){
      ROS_INFO_STREAM("Tolerances set correctly");
  }
  else{
      ROS_ERROR_STREAM("Failed to set tolerances");
      return -1;
  }

  // 3) Send command to execute trajectory
  std_srvs::Empty::Request call_req;
  std_srvs::Empty::Response call_resp;

  bool success_send = CallClient.call(call_req, call_resp);

  if(success_send){
      ROS_INFO_STREAM("Goal sent correctly. Executing.");
  }
  else{
      ROS_ERROR_STREAM("Failed to send goal");
      return -1;
  }

  return 0;
}

// Function to wait until the goal is reached
bool waitUntilGoalReached(ros::ServiceClient clt_check_mov_action){

  chess_implementation::CheckMovAction::Request check_mov_req;
  chess_implementation::CheckMovAction::Response check_mov_resp;

  bool finished = false;

  ROS_INFO_STREAM("Checking if follow joint action has finished...");
  while (!finished) {
    bool success_check_mov = clt_check_mov_action.call(check_mov_req, check_mov_resp);
    if (!success_check_mov) {
      ROS_ERROR_STREAM("Failed to check follow joint trajectory status");
      return false;
    }
    finished = check_mov_resp.finished;
  }
  if (check_mov_resp.error_code == 0) {
    ROS_INFO_STREAM("Finished with status: " << check_mov_resp.error_code);
  }
  else {
    ROS_ERROR_STREAM("Finished with status: " << check_mov_resp.error_code);
  }

  return true;
}

// Callbacks of the Dynamic Reconfigure input GUI
void DR_callback(chess_implementation::ChessInputConfig &config, uint32_t level) {

  // Only check if checkbox marked
  if (config.send_command_A){
    // Only affect if valid goal
    if (isValidCell(config.goal_location_A)) {
      command_A.pending = true;
      command_A.chess_piece = config.chess_piece_A;
      command_A.goal_cell = config.goal_location_A;

      config.goal_location_A = "";
    }
    else{
      ROS_ERROR_STREAM("Given goal cell is not valid, please try again!");
    }
    config.send_command_A = false;
  }
  // Same for team B (White)
  if (config.send_command_B){
    if (isValidCell(config.goal_location_B)) {
      command_B.pending = true;
      command_B.chess_piece = config.chess_piece_B;
      command_B.goal_cell = config.goal_location_B;

      config.goal_location_B = "";
    }
    else{
      ROS_ERROR_STREAM("Given goal cell is not valid, please try again!");
    }
    config.send_command_B = false;
  }
}

// -----------------------------------------------

// MAIN FUNCTION STARTS HERE
int main(int argc, char **argv) {

  // Initialize the ROS system and become a node.
  ros::init(argc, argv, "chess_manager");
  ros::NodeHandle n;

  // Define client objects to all services
  ros::ServiceClient clt_check_initial_positions = n.serviceClient<chess_implementation::CheckInitialPositions>("check_initial_positions");
  ros::ServiceClient clt_chess_piece_info = n.serviceClient<chess_implementation::ChessPieceInfo>("chess_piece_info");
  ros::ServiceClient clt_check_free_cells = n.serviceClient<chess_implementation::CheckFreeCells>("check_free_cells");
  ros::ServiceClient clt_check_content_cell = n.serviceClient<chess_implementation::CheckContentCell>("check_content_cell");
  ros::ServiceClient clt_planmovement = n.serviceClient<chesslab_setup::ffplan>("/chesslab_setup/planmovement");
  ros::ServiceClient clt_plan_chess_action = n.serviceClient<chess_implementation::PlanChessAction>("plan_chess_action");
  ros::ServiceClient clt_joint_lin_traj = n.serviceClient<chess_implementation::SetTrajectory>("joint_lin_traj");
  ros::ServiceClient clt_carte_lin_traj = n.serviceClient<chess_implementation::SetTrajectory>("carte_lin_traj");
  ros::ServiceClient clt_set_tolerance = n.serviceClient<chess_implementation::SetTolerance>("set_tolerance");
  ros::ServiceClient clt_call_action = n.serviceClient<std_srvs::Empty>("call_action");
  ros::ServiceClient clt_check_mov_action = n.serviceClient<chess_implementation::CheckMovAction>("check_mov_action");
  ros::ServiceClient clt_set_robot = n.serviceClient<chess_implementation::SetRobot>("set_robot");

  // Define dynamic reconfigure
  dynamic_reconfigure::Server<chess_implementation::ChessInputConfig> server;
  dynamic_reconfigure::Server<chess_implementation::ChessInputConfig>::CallbackType f;

  f = boost::bind(&DR_callback, _1, _2);
  server.setCallback(f);

  // Make sure all services are available
  ros::service::waitForService("check_initial_positions");
  ros::service::waitForService("chess_piece_info");
  ros::service::waitForService("check_free_cells");
  ros::service::waitForService("check_content_cell");
  ros::service::waitForService("/chesslab_setup/planmovement");
  ros::service::waitForService("plan_chess_action");
  ros::service::waitForService("joint_lin_traj");
  ros::service::waitForService("carte_lin_traj");
  ros::service::waitForService("set_tolerance");
  ros::service::waitForService("call_action");
  ros::service::waitForService("check_mov_action");
  ros::service::waitForService("set_robot");

  // By default we check the initial positions, but we may start in a random
  //   configuration when showing demos. This is indicated by the launch file
  bool check_initial_positions = true;
  if(!n.getParam("/chess_manager/check_initial_positions", check_initial_positions)) {
    ROS_ERROR("Need to define check_initial_positions parameter");
  }

  // -------------------------------------------
  // ------------  CHESS GAME CODE  ------------
  // -------------------------------------------

  // If chess game starts from scratch, wait until all pieces are correct
  if (check_initial_positions) {
    if(waitForInitialPositions(clt_check_initial_positions)) {
      ROS_INFO_STREAM(endl << endl << "All in order. OPEN THE GAME!" << endl);
    }
    else {
      ROS_ERROR_STREAM("Cannot start game");
      return -1;
    }
  }

  // Main variables
  bool game_over = false;
  string current_cell;
  int goal_content;
  string active_player = "WHITE"; // White starts the game
  chess_command active_command;
  int game_turn = 0;
  string robot_name;
  geometry_msgs::Quaternion aux_grasping_quat;


  // MAIN GAME LOOP UNTIL GAME OVER
  while (!game_over && n.ok()){

    // Assign command
    if (active_player == "WHITE") {
      game_turn++;

      // Show turn only if started from scratch
      if (check_initial_positions) {
        ROS_INFO_STREAM(endl << endl <<
          "CHESS GAME TURN: " << game_turn << endl);
      }

      // Team B plays White
      active_command = command_B;
      robot_name = "team_B";

      // Definition of auxiliar grasping orientation
      //  (a 90 deg rotation from default) for team B
      aux_grasping_quat.x = 0.7071;
      aux_grasping_quat.y = -0.7071;
      aux_grasping_quat.z = 0.0;
      aux_grasping_quat.w = 0.0;

      ROS_INFO_STREAM("WHITE TO MOVE: Waiting Team B command");
    }
    else { // == "BLACK"

      // Team A plays Black
      active_command = command_A;
      robot_name = "team_A";

      // Definition of auxiliar grasping orientation
      //  (a 90 deg rotation from default) for team A
      aux_grasping_quat.x = 0.7071;
      aux_grasping_quat.y = 0.7071;
      aux_grasping_quat.z = 0.0;
      aux_grasping_quat.w = 0.0;

      ROS_INFO_STREAM("BLACK TO MOVE: Waiting Team A command");
    }

    // Clear input before asking command
    active_command.pending = false;
    active_command.valid = false;
    command_A.pending = false;
    command_A.valid = false;
    command_B.pending = false;
    command_B.valid = false;

    // If command is invalid, must ask again and repeat
    while (!active_command.valid) {

      // Wait for command to be received
      while (!active_command.pending) {

        command_A.pending = false;
        command_B.pending = false;
        ros::spinOnce();

        // Both commands get updated, reassign
        if (active_player == "WHITE") {
          active_command = command_B;
        }
        else { // == "BLACK"
          active_command = command_A;
        }

        // If sent and valid goal -> pending
      }

      // Get chess piece info (current cell)
      chess_implementation::ChessPieceInfo::Request info_req;
      chess_implementation::ChessPieceInfo::Response info_resp;

      info_req.ID = active_command.chess_piece;

      bool success_info = clt_chess_piece_info.call(info_req, info_resp);

      if(success_info){
          ROS_INFO_STREAM("Got chess piece info");
          current_cell = info_resp.cell;
      }
      else{
          ROS_ERROR_STREAM("Failed to get chess piece info");
          return -1;
      }

      // Check if the command is a valid move depending on piece type
      bool chess_friendly = isValidChessMove(active_command.chess_piece, current_cell, active_command.goal_cell);
      if (chess_friendly) {
        ROS_INFO_STREAM("The command is in the valid moveset");
      }
      else{
        ROS_ERROR_STREAM("Chess piece cannot move this way, against the rules, please try again!");
        active_command.pending = false;
        continue;
      }

      // Check if the command is a valid move depending on path cells
      //   (need to be free if not moving a Knight)
      chess_implementation::CheckFreeCells::Request cells_req;
      chess_implementation::CheckFreeCells::Response cells_resp;

      cells_req.cells = getPathCells(active_command.chess_piece, current_cell, active_command.goal_cell);

      if (!cells_req.cells.empty()) {
        bool success_cells = clt_check_free_cells.call(cells_req, cells_resp);
        if(success_cells){
            ROS_INFO_STREAM("Checked free cells");
            if (cells_resp.all_free) {
              ROS_INFO_STREAM("The cells between the origin and destination are free");
            }
            else{
              ROS_ERROR_STREAM("Path cells not free. Against the rules, please try again!");
              active_command.pending = false;
              continue;
            }
        }
        else{
            ROS_ERROR_STREAM("Failed to get chess piece info");
            return -1;
        }
      }
      else{
        ROS_INFO_STREAM("No cells to check between origin and goal");
      }

      // Check if the command is a valid move depending on goal cell
      chess_implementation::CheckContentCell::Request goal_cont_req;
      chess_implementation::CheckContentCell::Response goal_cont_resp;

      goal_cont_req.cell = active_command.goal_cell;

      bool success_goal_cont = clt_check_content_cell.call(goal_cont_req, goal_cont_resp);

      if(success_goal_cont){
        ROS_INFO_STREAM("Got cell content");

        // Save goal content (might be own piece, enemy piece or free)
        goal_content = goal_cont_resp.chess_piece;

        // Check if in the goal cell there is an own piece
        if (active_player == "WHITE" && goal_content > 300) {
          // White playing with white piece on goal
          ROS_ERROR_STREAM("Goal cell has own piece. Against the rules, please try again!");
          active_command.pending = false;
          continue;
        }
        else if (active_player == "BLACK" && goal_content > 200 && goal_content < 300) {
          // Black playing with black piece on goal
          ROS_ERROR_STREAM("Goal cell has own piece. Against the rules, please try again!");
          active_command.pending = false;
          continue;
        }
        else{
          // No own piece on goal.

          // Check if the movement is valid if the piece is a pawn
          if (chess_piece_type[active_command.chess_piece] == active_player + " PAWN") {
            if (!isPawnMovValid(current_cell, active_command.goal_cell, goal_content)) {
              ROS_ERROR_STREAM("Invalid pawn movement, against the rules, please try again!");
              active_command.pending = false;
              continue;
            }
          }
        }
        ROS_INFO_STREAM("The destination cell content is correct");
      }
      else{
          ROS_ERROR_STREAM("Failed to get cell content");
          return -1;
      }

      ROS_INFO_STREAM("The command is valid -> Proceeding...");
      active_command.valid = true;
    }

    // Call ffplan service
    chesslab_setup::ffplan::Request plan_req;
    chesslab_setup::ffplan::Response plan_resp;

    plan_req.init.objarucoid.push_back(active_command.chess_piece);
    plan_req.init.occupiedcells.push_back(current_cell);

    // If goal cell is empty ...
    if (goal_content == 0) {
      plan_req.init.freecells.push_back(active_command.goal_cell);
    }
    // If goal cell has an opponent's chess piece
    else{
      plan_req.init.objarucoid.push_back(goal_content);
      plan_req.init.occupiedcells.push_back(active_command.goal_cell);

      plan_req.killed = goal_content;
    }

    plan_req.goal.objarucoid.push_back(active_command.chess_piece);
    plan_req.goal.occupiedcells.push_back(active_command.goal_cell);

    plan_req.goal.freecells.push_back(current_cell);

    bool success_plan = clt_planmovement.call(plan_req, plan_resp);

    if(success_plan){
        ROS_INFO_STREAM("Got chess plan");
    }
    else{
        ROS_ERROR_STREAM("Failed to get chess plan");
        return -1;
    }

    // Castling requested and possible:
    //  Command was King moving 2 cells from original position
    //  + King and Rook have not moved (castling_available=true)
    //  + No pieces in the middle cells or destination
    //  (Check/menace conditions not tracked)
    castling_rook = 0;
    if (is_castling_move && goal_content == 0) {
      ROS_INFO_STREAM("Adding Rook move (from "
              << castling_cells[0] << " to " << castling_cells[1]
              << ") to Castling plan");

      // Get Aruco ID of Rook to move
      //  (known cell, might be either ID depending on initial conditions)
      chess_implementation::CheckContentCell::Request rook_cont_req;
      chess_implementation::CheckContentCell::Response rook_cont_resp;

      rook_cont_req.cell = castling_cells[0];

      bool success_rook_cont = clt_check_content_cell.call(rook_cont_req, rook_cont_resp);
      if (success_rook_cont && rook_cont_resp.chess_piece != 0) {
        ROS_INFO_STREAM("Got Aruco ID of Rook to move");
        castling_rook = rook_cont_resp.chess_piece;
      }
      else{
        ROS_ERROR_STREAM("Failed to get Rook ID from cell");
        return -1;
      }

      // Call ffplan service again
      chesslab_setup::ffplan::Request plan_castling_req;
      chesslab_setup::ffplan::Response plan_castling_resp;

      // Initial state: Rook at corner, middle free
      plan_castling_req.init.objarucoid.push_back(castling_rook);
      plan_castling_req.init.occupiedcells.push_back(castling_cells[0]);
      plan_castling_req.init.freecells.push_back(castling_cells[1]);

      // Goal state: free and occupied the other way around
      plan_castling_req.goal.objarucoid.push_back(castling_rook);
      plan_castling_req.goal.occupiedcells.push_back(castling_cells[1]);
      plan_castling_req.goal.freecells.push_back(castling_cells[0]);

      bool success_rook_plan = clt_planmovement.call(plan_castling_req, plan_castling_resp);

      if(success_rook_plan){
          ROS_INFO_STREAM("Got castling Rook plan. Combining plans");

          // Add Rook plan steps to first part of plan (King movement)
          for (int k = 0; k < plan_castling_resp.plan.size(); k++) {
            plan_resp.plan.push_back(plan_castling_resp.plan[k]);
          }
      }
      else{
          ROS_ERROR_STREAM("Failed to get castling plan");
          return -1;
      }
    }

    // Set robot to move (team_A or team_B)
    chess_implementation::SetRobot::Request set_rob_req;
    chess_implementation::SetRobot::Response set_rob_resp;

    set_rob_req.robot_name = robot_name;

    bool success_set_rob = clt_set_robot.call(set_rob_req, set_rob_resp);

    if(success_set_rob){
        ROS_INFO_STREAM("Robot name has been set");
    }
    else{
        ROS_ERROR_STREAM("Failed to set robot name");
        return -1;
    }

    // Auxiliar orientation is off by default
    bool aux_grasp = false;
    int return_traj_code;

    chess_implementation::PlanChessAction::Request action_req;
    chess_implementation::PlanChessAction::Response action_resp;

    // Loop through all actions (pickup/putdown/throwaway) in plan
    for(int i = 0; i < plan_resp.plan.size(); i++) {
      action_req.action = plan_resp.plan[i];

      // Get partial goal sequence for each plan action
      bool success_action = clt_plan_chess_action.call(action_req, action_resp);

      if(success_action){
          ROS_INFO_STREAM("Planned chess action");
      }
      else{
          ROS_ERROR_STREAM("Failed to plan chess action");
          return -1;
      }

      // Piece model needed for collision checking
      string piece_to_move = action_resp.chess_piece;

      // Loop for all partial goals of the same action
      for (int j = 0; j < action_resp.partial_goals.size(); j++) {

        // Get partial goal (tcp & gripper)
        chess_implementation::RobotConfig partial_goal = action_resp.partial_goals[j];

        if (aux_grasp) {
          partial_goal.tcp.orientation = aux_grasping_quat;
        }
        // else keep the original orientation (no need to impose that)

        // First action of the plan: home to safety in joint space
        if (i==0 && j==0) {
          return_traj_code = executeLinTraj(partial_goal, piece_to_move,
                                          false, robot_name, clt_joint_lin_traj,
                                          clt_set_tolerance, clt_call_action);

          if (return_traj_code != 0) {
            ROS_ERROR_STREAM("Failed to execute movement");
            return -1;
          }

          ROS_INFO_STREAM("Initial movement started correctly");

          if(!waitUntilGoalReached(clt_check_mov_action)) {
            return -1;
          }
        }
        // All other actions have to be linear in the cartesian/operational space
        else{
          return_traj_code = executeLinTraj(partial_goal, piece_to_move,
                                          false, robot_name, clt_carte_lin_traj,
                                          clt_set_tolerance, clt_call_action);


          if (return_traj_code == -1) {
            ROS_ERROR_STREAM("Failed to execute movement");
            return -1;
          }
          else if (return_traj_code == 0) {
            ROS_INFO_STREAM("Movement started correctly");
            if(!waitUntilGoalReached(clt_check_mov_action)) {
              return -1;
            }
          }
          else { // return_traj_code == 1
            ROS_WARN("Collision detected, setting alternative grasp and trying again...");

            chess_implementation::RobotConfig new_partial_goal = action_resp.partial_goals[j-1];

            // Toggle auxiliar grasp
            aux_grasp = !aux_grasp;
            if (aux_grasp) {
              new_partial_goal.tcp.orientation = aux_grasping_quat;
            }

            return_traj_code = executeLinTraj(new_partial_goal, piece_to_move,
                                            false, robot_name, clt_carte_lin_traj,
                                            clt_set_tolerance, clt_call_action);

            if (return_traj_code != 0) {
              ROS_ERROR_STREAM("Failed to execute movement");
              return -1;
            }

            ROS_INFO_STREAM("Movement started correctly");

            if(!waitUntilGoalReached(clt_check_mov_action)) {
              return -1;
            }

            // Change orientation if needed
            if (aux_grasp) {
              partial_goal.tcp.orientation = aux_grasping_quat;
            }
            else{
              partial_goal.tcp.orientation = action_resp.partial_goals[j].tcp.orientation;
            }

            return_traj_code = executeLinTraj(partial_goal, piece_to_move,
                                            false, robot_name, clt_carte_lin_traj,
                                            clt_set_tolerance, clt_call_action);

            if (return_traj_code != 0) {
              ROS_ERROR_STREAM("Failed to execute movement");
              return -1;
            }

            ROS_INFO_STREAM("Movement started correctly");

            if(!waitUntilGoalReached(clt_check_mov_action)) {
              return -1;
            }
          }
        }
      }
    }

    // Additional goal not in plan: return home
    // Set empty partial goal, returning home does not need it
    chess_implementation::RobotConfig partial_goal;
    string piece_to_move;

    return_traj_code = executeLinTraj(partial_goal, piece_to_move,
                                    true, robot_name, clt_joint_lin_traj,
                                    clt_set_tolerance, clt_call_action);

    if (return_traj_code != 0) {
      ROS_ERROR_STREAM("Failed to execute movement");
      return -1;
    }

    ROS_INFO_STREAM("Last movement started correctly");

    if(!waitUntilGoalReached(clt_check_mov_action)) {
      return -1;
    }

    // INHABILITATE CASTLING IF ROOK/KING MOVED
    // First, check Rook movements
    if (current_cell == "A1") {
      // Moved a piece on White Queenside corner
      if (active_command.chess_piece == 309 || active_command.chess_piece == 310) {
        // The piece was a White Rook
        castling_available["WHITE QUEENSIDE"] = false;
        ROS_WARN_STREAM("White Queenside Castling no longer available");
      }
    }
    else if (current_cell == "H1") {
      // Moved a piece on White Kingside corner
      if (active_command.chess_piece == 309 || active_command.chess_piece == 310) {
        // The piece was a White Rook
        castling_available["WHITE KINGSIDE"] = false;
        ROS_WARN_STREAM("White Kingside Castling no longer available");
      }
    }
    else if (current_cell == "A8") {
      // Moved a piece on Black Queenside corner
      if (active_command.chess_piece == 209 || active_command.chess_piece == 210) {
        // The piece was a Black Rook
        castling_available["BLACK QUEENSIDE"] = false;
        ROS_WARN_STREAM("Black Queenside Castling no longer available");
      }
    }
    else if (current_cell == "H8") {
      // Moved a piece on Black Kingside corner
      if (active_command.chess_piece == 209 || active_command.chess_piece == 210) {
        // The piece was a Black Rook
        castling_available["BLACK KINGSIDE"] = false;
        ROS_WARN_STREAM("Black Kingside Castling no longer available");
      }
    }

    // Second, check if Kings moved
    if (active_command.chess_piece == 316) {
      castling_available["WHITE QUEENSIDE"] = false;
      castling_available["WHITE KINGSIDE"] = false;
      ROS_WARN_STREAM("White King Castling no longer available on any side");
    }
    else if (active_command.chess_piece == 216) {
      castling_available["BLACK QUEENSIDE"] = false;
      castling_available["BLACK KINGSIDE"] = false;
      ROS_WARN_STREAM("Black King Castling no longer available on any side");
    }

    // CHECK IF ARUCO IN DESIRED FINAL CELL
    // First, get location of moved piece
    chess_implementation::ChessPieceInfo::Request info_fin_req;
    chess_implementation::ChessPieceInfo::Response info_fin_resp;

    info_fin_req.ID = active_command.chess_piece;

    bool success_info_fin = clt_chess_piece_info.call(info_fin_req, info_fin_resp);

    // If we get response, check if the piece is now on the desired goal
    if(success_info_fin){
        ROS_INFO_STREAM("Got chess piece final info");
        if (info_fin_resp.cell == active_command.goal_cell) {
          ROS_INFO_STREAM("Movement checked: Piece " << active_command.chess_piece
                                << " is now in Cell " << active_command.goal_cell);
        }
        else{
          ROS_WARN_STREAM("Movement had unexpected result. Piece " << active_command.chess_piece
                       << " should be in Cell " << active_command.goal_cell
                       << ", but has location: " << info_fin_resp.cell);
        }
    }
    else{
        ROS_ERROR_STREAM("Failed to check movement result");
        return -1;
    }

    // CHECK FOR GAME OVER
    // First, get location of opponent's King
    chess_implementation::ChessPieceInfo::Request info_K_req;
    chess_implementation::ChessPieceInfo::Response info_K_resp;

    string game_continues_txt;
    stringstream game_over_txt;

    if (active_player == "WHITE") {
      info_K_req.ID = 216; // Black King (enemy)
      game_continues_txt = "The Black King lives. The game continues...";
      game_over_txt << "The Black King is dead" << endl << endl
                    << " ~~ WHITE WINS! ~~ " << endl;
    }
    else { // == "BLACK"
      info_K_req.ID = 316; // White King (enemy)
      game_continues_txt = "The White King lives. The game continues...";
      game_over_txt << "The White King is dead" << endl << endl
                    << " ~~ BLACK WINS! ~~ " << endl;
    }

    bool success_info_K = clt_chess_piece_info.call(info_K_req, info_K_resp);

    // If we get response, check if it still lives
    if(success_info_K){
        ROS_INFO_STREAM("Got King info");
        if (info_K_resp.cell != "OUT") {
          ROS_INFO_STREAM(game_continues_txt);
        }
        else{
          ROS_WARN_STREAM(game_over_txt.str());
          game_over = true;
          //continue
        }
    }
    else{
        ROS_ERROR_STREAM("Failed to get King info");
        return -1;
    }

    // Change player for next move
    if (active_player == "WHITE") {
      active_player = "BLACK";
    }
    else {
      active_player = "WHITE";
    }

  }

  // Game ended correctly
  // ROS_INFO_STREAM("GGWP. Thanks for playing!")
  return 0;

}
