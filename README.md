# Introduction to ROS - Final Project: Chess
---

## Overview

This repository includes all the files related to the _Introduction to ROS_ Final Project, developed by **Group 10: Albert Bhagwan & Adrià Luque**.

The statement and information about other dependencies can be found on the [Final Work Page](https://sir.upc.edu/projects/rostutorials/final_work/index.html)

In this repository we include:

* Our complete source code, including several demos, described in this readme.
* Both the pdfs for the final presentation and project report in the _docs_ folder
* Videos (screen captures) of all the given demos, sped up x8

---

## Execute Full Game

The main code of our project includes the simulation of a full chess game starting with all pieces on their initial positions. It can be launched with:

`roslaunch chess_implementation complete_sim.launch`

This launch file uses the `chesslab.world` file to set the scene in Gazebo, and follows the flow described in our report.

An example of the first turn of a chess game (one move from each player) can be seen in _/videos/Chess Game - First Turn_.


---

## Execute Demos

We have included four additional demos, all with their corresponding launch, world and additional cpp files. They are the following:

* Killing move demo
* Collision detection and handling demo
* Castling move demo
* Endgame demo

**Important:** To execute any of these demos, one must wait until the setup is initialized. This means that all chess pieces in Rviz have been moved to their correct initial positions, that must be the same as in Gazebo.   


### Killing move demo

This demo is designed to showcase a killing move, and can be launched with:

`roslaunch chess_implementation demo_kill_sim.launch`

Which uses the `demo_kill.world` scene to set the scene in Gazebo and the `demo_kill.cpp` to match the initial configuration of the pieces in Rviz. Remember to wait for this code to finish before sending a command.

This initial configuration can be seen on the following figure, together with the associated move to execute:

![DemoKilling](https://i.imgur.com/soUvqUo.png)

The input is WHITE PAWN 1 to F6. The result can also be seen in _/videos/Demo - Killing Move.mp4_.


### Collision detection and handling demo

This demo is designed to showcase a move where the gripper would collide with a nearby piece on the pick operation, how it rotates, and how it has to go back to the original orientation before placing the piece, as it would collide with another one near the goal cell. It can be launched with:

`roslaunch chess_implementation demo_rot_grasp.launch`

Which uses the `demo_rot_grasp.world` scene to set the scene in Gazebo and the `demo_rot_grasp.cpp` to match the initial configuration of the pieces in Rviz. Remember to wait for this code to finish before sending a command.

This initial configuration can be seen on the following figure, together with the associated move to execute:

![DemoCollision](https://i.imgur.com/d0MeaHx.png)

The input is WHITE PAWN 4 to E4. The result can also be seen in _/videos/Demo - Collision Avoidance.mp4_.


### Castling move demo

This demo is designed to showcase a castling move, concretely a Queenside Castling of the White King. It can be launched with:

`roslaunch chess_implementation demo_castling.launch`

Which uses the `demo_castling.world` scene to set the scene in Gazebo and the `demo_castling.cpp` to match the initial configuration of the pieces in Rviz. This setup is completely artificial, as only the required pieces are removed. Remember to wait for this code to finish before sending a command.

This initial configuration can be seen on the following figure, together with the associated move to execute:

![DemoCastling](https://i.imgur.com/69a9x6B.png)

The input is WHITE KING to C1. The Rook movement is added automatically. The result can also be seen in _/videos/Demo - Castling Move.mp4_.


### Endgame demo

This demo is designed to showcase the ending of a chess game, starting just before killing a king. It can be launched with:

`roslaunch chess_implementation demo_endgame.launch`

Which uses the `demo_endgame.world` scene to set the scene in Gazebo and the `demo_endgame.cpp` to match the initial configuration of the pieces in Rviz. This setup does not represent a real advanced game almost finishing. Remember to wait for this code to finish before sending a command.

This initial configuration can be seen on the following figure, together with the associated move to execute:

![DemoEndgame](https://i.imgur.com/yAwMjMr.png)

The input is WHITE QUEEN to E5. The result can also be seen in _/videos/Demo - Endgame.mp4_.

---

## Execute Sensing Simulation

We have an additional launch file related with the Sensing Module, which can be launched with:

`roslaunch chess_implementation sensing_sim.launch`

This is a partial simulation including only the Sensing Module, to test its services.

---

***Albert Bhagwan & Adrià Luque***

*ETSEIB, UPC. 2020*

![ETSEIB](https://frm.eic.cat/se/img/logo-ETSEIB-UPC.png)
